;++
;===================================================================================
;///////////////////////////////////////////////////////////////////////////////////
;
; GENERAL DESCRIPTION
;
; NAME:
;	ogee_solar_parameters
;
; PURPOSE:
; 	Function to compute sun height (degrees), azimuth (degrees)
; 	and extra-atmospheric radiation (W/m2) at location
; 	(latitude,longitude) for year (yyyy), jday (0-366) and time (0-24)
;
; CALLING SEQUENCE:
;	ogee_solar_parameters,$
;           type=type,$
;           latitude, $
;           longitude, $
;           year, $
;           jday, $
;           time, $
;           rg_extatm,$
;           sun_height, $
;           time2GMT=time2GMT, $
;           sun_azim=sun_azim, $
;           dmean_to_d_ratio=dmean_to_d_ratio,$
;           xdecl=xdecl,$
;           eqt=eqt,$
;           coszen=coszen
;
; INPUTS:
;	latitude = latitude of location (from -90 to +90)
;       longitude = longitude of location (from -180 to +180)
;       year = year (negative if BC)
;       jday = day of year (0-366)
;       time = local time of day (0-24)
;
;---
; OPTIONAL INPUT PARAMETERS:
;	type = 0 same code as in MuSICA, ie, from Charles Valancogne routines (default)
;              1 same as in rg_surface_clearsky.pro routine
;              2 same as on website http://solardat.uoregon.edu/SolarRadiationBasics.html
;       time2GMT = time difference between time and GMT (h)
;                  i.e., time2GMT = time - GMT
;                  i.e., anything between -12h and +12h
;                  expected to be positive if longitude > 0
;                  IF NOT PROVIDED WE ASSUME time IS GMT
;
; OUTPUTS:
;	sun_height = solar elevation (0-90 degrees)
;       rg_extatm = extra-atmospheric solar radiation (W/m2)
;
; OPTIONAL OUTPUTS:
;       sun_azim = solar azimuth (degrees)
;       dmean_to_d_ratio = mean to actual Sun-Earth distance ratio (-)
;       xdecl = solar declination (degrees)
;       eqt = equation of time (minutes)
;       coszen = cosinus of zenith angle (-)
;
; RESTRICTIONS:
;	If sun_azim is set then type=0
;
; DOCUMENTATION:
;
;--- see: http://solardat.uoregon.edu/SolarRadiationBasics.html
;
;-----------------------------------------------------------------------------------
;--- Extra-terrestrial solar radiation (Io) is the radiation
;    from the Sun to the Earth on a plane perpendicular to 
;    the Sun-Earth line
;
;    Io = 1367 * (Rav/R)^2 ;--- W/m2
;
;    where Rav is the average Sun-Earth distance
;    and R is the actual Sun-Earth distance.
;    (Rav/R)^2 is typically close to unity +/- 3%.
;    We have the approximation:
;
;    (Rav/R)^2 = 1.00011 $
;              + 0.034221 * cos(  orbit) $
;              + 0.001280 * sin(  orbit) $
;              + 0.000719 * cos(2*orbit) $
;              + 0.000077 * sin(2*orbit)
;
;    where orbit=2*!pi*jday/365.2425.
;
;    Other expressions are:
;
;    (Rav/R)^2 = 1 $
;              + 2*eccentricity*COS(omega_earth*(jday_x - jday_perihelion))
;
;    with:
;
;    omega_earth     =  2.*!pi/365.25636
;    eccentricity    =  0.016751 - 0.42000E-6*(year-1900)
;    jday_x = jday + 0.5
;    long_perihelion = -1.374953 + 3.00051E-4*(year-1900)
;    jday_equinox    =  80.08000 + 0.24220   *(year-1900) $
;                    - float(fix((year-1901)/4.))
;    jday_perihelion = jday_equinox  + (long_perihelion $
;                    - 2.*eccentricity*SIN(long_perihelion)) / omega_earth
;
;-----------------------------------------------------------------------------------
;--- Declination (xdecl) is the angle between a plane perpendicular
;    to the Sun-Earth line and the Earth's rotaional axis. We have
;    the approximation:
;
;    xdecl = 23.45 * !pi/180. * sin(2*!pi * (284 + jday) / 365)
;
;    The 23.45 is the angle between the Earth's rotational axis
;    and the Earth's orbital plane around the Sun.
;    This expression can be re-written:
;
;    xdecl = 0.000000 - $
;            0.402925  * cos(orbit) + $
;            0.0718417 * sin(orbit)
;
;    Other possible expressions are:
;
;    xdecl =  0.006918             - $
;             0.399912 * cos(orbit)       + $
;             0.070257 * sin(orbit)       - $
;             0.006758 * cos(2.0 * orbit) + $
;             0.000907 * sin(2.0 * orbit) - $
;             0.002697 * cos(3.0 * orbit) + $
;             0.001480 * sin(3.0 * orbit)
;
;    or:
;
;    xdecl = ASIN(SIN(celestial_longitude) * SIN(obliquity))
;
;   with:
;
;   obliquity =  0.409320 - 2.27100E-6*(year-1900)
;   celestial_longitude = long_perihelion + $
;                         omega_earth * (jday_x - jday_perihelion) + $
;                         2. * eccentricity * SIN(omega_earth * (jday_x - jday_perihelion))
;
;-----------------------------------------------------------------------------------
;--- Equation of time (Eqt)
;    The Equation of Time is the difference between the Mean Solar
;    Time (see below) and the real solar time (see below).
;    This varies between a minimum of -15 and a maximum of +15 minutes
;    during the year and can be approximated by (units are minutes):
;
;    Eqt = -14.2*sin(!pi*(jday +   7) / 111) for year day n between 1 and 106
;    Eqt =   4.0*sin(!pi*(jday - 106) /  59) for year day n between 107 and 166
;    Eqt =  -6.5*sin(!pi*(jday - 166) /  80) for year day n between 167 and 246
;    Eqt =  16.4*sin(!pi*(jday - 247) / 113) for year day n between 247 and 365
;
;-----------------------------------------------------------------------------------
;--- Real solar time (Tsolar) relates to the position of the
;    Sun with respect to an observer, as indicated by a sundial.
;    Mean solar time (Tmean) would be the position of the Sun if its
;    apparent movement was regular (i.e. Eqt=0)
;    GMT (or UTC) is mean solar time at Greenwich
;    To adjust mean solar time for longitude one must add 
;    (Long_local - Long_GMT)/15 (units are hours) from the
;    local time. Because LongGMT=0 and GMT = time - time2GMT
;    we have:
;
;    Tsolar = Tmean                              + Eqt / 60.
;           = GMT             +  Long_local/ 15. + Eqt / 60.
;           = time - time2GMT +  Long_local/ 15. + Eqt / 60.
;
;-----------------------------------------------------------------------------------
;--- Cosine of zenith angle (coszen)
;    To calculate the amount of solar radiation on a horizontal
;    surface, we need to multiply the normal irrandiance (Io) by the
;    cosine of the zenith angle.
;    The latter is given by:
;
;    coszen = sin(Lat) * sin(xdecl) + cos(Lat) * cos(xdecl) * cos(angle)
;
;    where Lat is the latitude of the observer and angle is the solar
;    time "angle":
;
;    angle = !pi * (Tsolar - 12.) / 12.
;
;    The -12. is because the Sun beam is downwards...
;    Note that if coszen is negative, then the radiation should be zero.
;    Also the Sun height above horizon (sun_height) is simply given
;    by:
;
;    sun_height = 180/!pi*ASIN(coszen)
;
;-----------------------------------------------------------------------------------
;///////////////////////////////////////////////////////////////////////////////////
;===================================================================================

PRO ogee_solar_parameters,$
   type=type,$
   latitude, $
   longitude, $
   year, $
   jday, $
   time, $
   rg_extatm,$
   sun_height, $
   time2GMT=time2GMT, $
   sun_azim=sun_azim, $
   dmean_to_d_ratio=dmean_to_d_ratio,$
   xdecl=xdecl,$
   eqt=eqt,$
   coszen=coszen

;--- Check options
IF SIZE(type,/type) EQ 0 OR SIZE(sun_azim,/type) NE 0 THEN type=0
IF SIZE(time2GMT,/type) EQ 0 THEN time2GMT=0

;--- Extra-terrestrial solar radiation (solar_constant)
solar_constant0 = 1370.
CASE type OF
   0: BEGIN ;- as in Charles Valancogne routines
      omega_earth     =  2.*!pi/365.25636
      eccentricity    =  0.016751 - 0.42000e-6*(year-1900)
      long_perihelion = -1.374953 + 3.00051e-4*(year-1900)
      jday_equinox    =  80.08000 + 0.24220   *(year-1900) - float(fix((year-1901)/4.))
      jday_perihelion = jday_equinox  + (long_perihelion $
                                         - 2.*eccentricity*SIN(long_perihelion)) / omega_earth
      jday_x = jday + 0.5
      dmean_to_d_ratio =  1. + $
                          2.*eccentricity*COS(omega_earth*(jday_x - jday_perihelion))
   END
   1: BEGIN ;- as in rg_surface_clearsky.pro routine
      orbit=2*!pi*jday/365.2425
      dmean_to_d_ratio =  (1.000110       + $
                           0.034221 * cos(orbit)        + $
                           0.001280 * sin(orbit)        + $
                           0.000719 * cos(2.0 * orbit)  + $
                           0.000077 * sin(2.0 * orbit))
   END
   2: BEGIN ;- as in website
      orbit=2*!pi*jday/365.2425
      dmean_to_d_ratio =  (1.000110       + $
                           0.034221 * cos(orbit)        + $
                           0.001280 * sin(orbit)        + $
                           0.000719 * cos(2.0 * orbit)  + $
                           0.000077 * sin(2.0 * orbit))
   END
ENDCASE
;---
dmean_to_d_ratio = SQRT(dmean_to_d_ratio)
solar_constant = solar_constant0 * dmean_to_d_ratio^2

;--- Earth declination angle (xdecl)
CASE type OF
   0: BEGIN ;- as in Charles Valancogne routines
      obliquity =  0.409320 - 2.27100e-6*(year-1900)
      celestial_longitude = long_perihelion + $
                            omega_earth * (jday_x - jday_perihelion) + $
                            2. * eccentricity * SIN(omega_earth * (jday_x - jday_perihelion))
      xdecl = ASIN(SIN(celestial_longitude) * SIN(obliquity))
   END
   1: BEGIN ;- as in rg_surface_clearsky.pro routine
      xdecl =  0.006918             - $
             0.399912 * cos(orbit)       + $
             0.070257 * sin(orbit)       - $
             0.006758 * cos(2.0 * orbit) + $
             0.000907 * sin(2.0 * orbit) - $
             0.002697 * cos(3.0 * orbit) + $
             0.001480 * sin(3.0 * orbit)
   END
   2: BEGIN ;- as in website
      xdecl = 23.45 * !pi/180. * sin(2*!pi * (284 + jday) / 365)
   END
ENDCASE

;---  Equation of time (eqt)
CASE type OF
   0: BEGIN ;- as in Charles Valancogne routines (note: opposite sign in original version...)
      eqt = +720./!pi * (ATAN(TAN(celestial_longitude) * COS(obliquity)) - omega_earth * (jday_x - jday_perihelion) - long_perihelion)
      FOR i=0L,N_ELEMENTS(eqt)-1 DO WHILE (eqt[i] LE -100.) DO eqt[i] = eqt[i] + 720.
      eqt = - eqt
   END
   1: BEGIN ;- as in rg_surface_clearsky.pro routine
      eqt =  REPLICATE(0.0,N_ELEMENTS(jday))
   END
   2: BEGIN ;- as in website
      eqt = FLTARR(N_ELEMENTS(jday))
      ii = WHERE(jday LE 106.,nii)                 & IF nii GT 0 THEN eqt[ii] = -14.2*SIN(!pi*(jday[ii] +   7.) / 111.)
      ii = WHERE(jday GE 107. AND jday LE 166,nii) & IF nii GT 0 THEN eqt[ii] =   4.0*SIN(!pi*(jday[ii] - 106.) /  59.)
      ii = WHERE(jday GE 167. AND jday LE 246,nii) & IF nii GT 0 THEN eqt[ii] =  -6.5*SIN(!pi*(jday[ii] - 166.) /  80.)
      ii = WHERE(jday GE 247. AND jday LE 366,nii) & IF nii GT 0 THEN eqt[ii] =  16.4*SIN(!pi*(jday[ii] - 247.) / 113.)
   END
ENDCASE

;---  Cosine of zenith angle (coszen), sun_height and 
solar_time = time - time2GMT + longitude/15. + eqt/60.
solar_time_angle = (solar_time - 12.) * !pi / 12.
latitude_rad = latitude * !pi / 180.0
CASE type OF
   0: BEGIN ;- as in Charles Valancogne routines
      ksi = SIN(latitude_rad) * SIN(xdecl) + COS(latitude_rad) * COS(xdecl) * COS(solar_time_angle)
      sun_height = FLTARR(n_elements(ksi))
      coszen = FLTARR(n_elements(ksi))
      ii = where(ksi LT 0.,nii)
      IF nii GT 0 THEN BEGIN
         sun_height[ii] = -99.9
         coszen[ii] = 0.
      ENDIF
      ii = where(ksi EQ 1.,nii)
      IF nii GT 0 THEN BEGIN
         sun_height[ii] = 90.
         coszen[ii] = 1.
      ENDIF
      ii = where(ksi GE 0. AND ksi NE 1.,nii)
      IF nii GT 0 THEN BEGIN
         sun_height[ii] = 180./!pi * ATAN(ksi[ii]/SQRT(1.-ksi[ii]*ksi[ii]))
         coszen[ii] = SIN(!pi/180.*sun_height[ii])
      ENDIF
   END
   1: BEGIN ;- as in rg_surface_clearsky.pro routine
      coszen = (SIN(latitude_rad) * SIN(xdecl) + COS(latitude_rad) * COS(xdecl) * COS(solar_time_angle)) > 0.
      sun_height = 180/!pi*ASIN(coszen)
      ii = WHERE(sun_height LE 0.,nii) & IF nii GT 0 THEN sun_height[ii] = -99.9
   END
   2: BEGIN ;- as in website
      coszen = (SIN(latitude_rad) * SIN(xdecl) + COS(latitude_rad) * COS(xdecl) * COS(solar_time_angle)) > 0.
      sun_height = 180/!pi*ASIN(coszen)
      ii = WHERE(sun_height LE 0.,nii) & IF nii GT 0 THEN sun_height[ii] = -99.9
   END
ENDCASE
rg_extatm = solar_constant * coszen

;--- sun azimuth
IF SIZE(sun_azim,/type) NE 0 THEN BEGIN
   ksi = SIN(latitude_rad) * COS(solar_time_angle)  - COS(latitude_rad) * TAN(xdecl)
   alfa = fltarr(n_elements(ksi))
   beta = fltarr(n_elements(ksi))
   ii = where(ksi lt 0.,nii)
   if nii gt 0 then begin
      alfa[ii] = 180./!pi*ATAN(SIN(solar_time_angle[ii])/ksi[ii])
      beta[ii] = 180.
   endif
   ii = where(ksi eq 0.,nii)
   if nii gt 0 then begin
      alfa[ii] = 0.
      beta[ii] = 90.
   endif
   ii = where(ksi gt 0,nii)
   if nii gt 0 then begin
      alfa[ii] = 180./!pi*ATAN(SIN(solar_time_angle[ii])/ksi[ii])
      beta[ii] = 0.
   endif
   ;---
   sun_azim = fltarr(n_elements(ksi))
   ii = where(solar_time_angle lt 0.,nii)
   if nii gt 0 then sun_azim[ii] = alfa[ii] - beta[ii]
   ii = where(solar_time_angle eq 0.,nii)
   if nii gt 0 then sun_azim[ii] = 0.
   ii = where(solar_time_angle gt 0.,nii)
   if nii gt 0 then sun_azim[ii] = alfa[ii] + beta[ii]
ENDIF

END
;+-
