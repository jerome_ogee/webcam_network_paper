;+
; NAME:
;    MC_SYMBOL
;
; PURPOSE:
;     Create user defined plotting symbol.
;
; CATEGORY:
;    Graphics.
;
; CALLING SEQUENCE:
;       mc_symbol, sym=sym, fill=fill, thick=thick, help=help
;
; INPUTS:
;    sym:    Integer specifying the symbol type (see below).
;    *** MC_SYMBOL TYPE ***
;    1         ->    square
;    2         ->    circle
;    3         ->    triangle
;    4         ->    triangle (inverted)
;    5         ->    diamond
;    6         ->    star 1
;    7         ->    star 2
;    8         ->    hourglass
;    9         ->    bowtie
;    10        ->    hourglass tilted left
;    11        ->    hourglass tilted right
;    12        ->    rigth half circel
;    13        ->    left half circel
;    14        ->    upper half circel
;    15        ->    lower half circel
;    16        ->    standing bar
;    17        ->    laying bar
;    18        ->    hat up
;    19        ->    hat down
;    20        ->    hat right
;    21        ->    hat left
;    22        ->    big cross
;    23        ->    circle w/ plus
;    24        ->    circle w/ ex
;    25        ->    partial arrow (right)
;    26        ->    partial arrow (left)
;    27        ->    full arrow
;    28        ->    vertical line
;    29        ->    horizontal line
;    30        ->    x
;    31        ->    +
;
; OPTIONAL INPUT PARAMETERS:
;    fill:    If specified then symbol is filled.
;    thick:    Specifies the line thickness of symbol.  Default:  thick=1.0
;
; OUTPUTS:
;    Defines IDL USERSYMBOL PSYM=8.
;
; COMMON BLOCKS:
;    None.
;
; SIDE EFFECTS:
;    None.
;
; RESTRICTIONS:
;    None.
;
; PROCEDURE:
;        MC_SYMBOL,sym=5            <- open diamond
;        PLOT,x,y,PSYM=8,COLOR=col
;        MC_SYMBOL,sym=2,fill=1            <- filled circle
;        PLOT,x,y,PSYM=8,SYMSIZE=.8,COLOR=col
;        
; MODIFICATION HISTORY:
;    Written, KAM, Apr. 1993.
;       Modified, MC, Mar. 2002 - include sym=0 as no symbol; sym=0 is a tiny little square
;                 MC, July 2002 - 11 new symbols which can be filled
;                               - change order: all fillable symbols first
;                               - fallback is sym=0
;-
PRO MC_SYMBOL, sym=sym, fill=fill, thick=thick, help=help
  COMPILE_OPT idl2
  ON_ERROR,2
  ;
  if keyword_set(help) then begin
      print,"mc_symbol, sym=sym, /fill, thick=thick, /help"
      print,"Symbol types: "
      print,"  sym=1         ->    square"
      print,"  sym=2         ->    circle"
      print,"  sym=3         ->    triangle"
      print,"  sym=4         ->    triangle (inverted)"
      print,"  sym=5         ->    diamond"
      print,"  sym=6         ->    star 1"
      print,"  sym=7         ->    star 2"
      print,"  sym=8         ->    hourglass"
      print,"  sym=9         ->    bowtie"
      print,"  sym=10        ->    hourglass tilted left"
      print,"  sym=11        ->    hourglass tilted right"
      print,"  sym=12        ->    rigth half circel"
      print,"  sym=13        ->    left half circel"
      print,"  sym=14        ->    upper half circel"
      print,"  sym=15        ->    lower half circel"
      print,"  sym=16        ->    standing bar"
      print,"  sym=17        ->    laying bar"
      print,"  sym=18        ->    hat up"
      print,"  sym=19        ->    hat down"
      print,"  sym=20        ->    hat right"
      print,"  sym=21        ->    hat left"
      print,"  sym=22        ->    big cross"
      print,"  sym=23        ->    circle w/ plus"
      print,"  sym=24        ->    circle w/ ex"
      print,"  sym=25        ->    partial arrow (right)"
      print,"  sym=26        ->    partial arrow (left)"
      print,"  sym=27        ->    full arrow"
      print,"  sym=28        ->    vertical line"
      print,"  sym=29        ->    horizontal line"
      print,"  sym=30        ->    x"
      print,"  sym=31        ->	   +"
      print,"include fill=1 for filled symbol, thick=thick for line thickness."
      return
  endif
  ;
  ; For sym=0
  if n_elements(sym) eq 0 then sym=0 else sym=fix(sym)
  if not keyword_set(fill) then fill=0
  if n_elements(thick) eq 0 then thick=1.
  ;
  case sym of
      ;
      ; tiny little square
      ;
      0: begin
          x=[-1,-1, 1, 1,-1]
          y=[-1, 1, 1,-1,-1]
          x=x/10000.
          y=y/10000.
      end 
      ;
      ; square
      ;
      1: begin
          x=[-1,-1, 1, 1,-1]
          y=[-1, 1, 1,-1,-1]
      end 
      ;
      ; circle
      ;
      2: begin
          x=[1,.866, .707, .500, 0,-.500,-.707,-.866,-1,$
             -.866,-.707,-.500, 0, .500, .707, .866, 1]
          y=[0,.500, .707, .866, 1, .866, .707, .500, 0,$
             -.500,-.707,-.866,-1,-.866,-.707,-.500, 0]
      end 
      ;
      ; triangle
      ;
      3: begin
          x=[-0.924, 0.000, 0.924,-0.924]
          y=[-0.600, 1.000,-0.600,-0.600]
      end
                                  ;
      ; inverted triangle
      ;
      4: begin
          x=[-0.924, 0.000, 0.924,-0.924]
          y=[ 0.600,-1.000, 0.600, 0.600]
      end
      ;
      ; diamond
      ;
      5: begin
          x=[ 0,-1, 0, 1, 0]
          y=[-1, 0, 1, 0,-1]
      end
      ;
      ; star 2
      ;
      6: begin
          x=[-1,-.2, 0,.2,1, .2, 0,-.2,-1]
          y=[ 0, .2, 1,.2,0,-.2,-1,-.2, 0]
      end
      ;
      ; star 1
      ;
      7: begin
          x=[ 0, .4,  1, .7,  1, .4, 0, -.4, -1, -.7,  -1, -.4, 0]
          y=[-1,-.4,-.4,  0, .4, .4, 1,  .4, .4,   0, -.4, -.4,-1]
      end
      ;
      ; hourglass
      ;
      8: begin
          x=[-1, 1,-1,1,-1]
          y=[-1,-1, 1,1,-1]
      end
      ;
      ; hourglass (on side)
      ;
      9: begin
          x=[-1,-1, 1,1,-1]
          y=[-1, 1,-1,1,-1]
      end
      ;
      ; hourglass tilted left
      ;
      10: begin
          x=[-1, 0, 0,-1, 1, 0, 0, 1,-1]
          y=[ 0, 1, 0, 0, 0,-1, 0, 0, 0]
      end
      ;
      ; hourglass tilted right
      ;
      11: begin
          x=[-1, 1, 0, 0,-1]
          y=[ 0, 0, 1,-1, 0]
      end
      ;
      ; rigth half circel
      ;
      12: begin
          x=[1,.866, .707, .500, 0,$
             0, .500, .707, .866, 1]
          y=[0,.500, .707, .866, 1,$
             -1,-.866,-.707,-.500, 0]
      end
      ;
      ; left half circel
      ;
      13: begin
          x=[0, 0,-.500,-.707,-.866,-1,$
             -.866,-.707,-.500, 0]
          y=[-1, 1, .866, .707, .500, 0,$
             -.500,-.707,-.866,-1]
      end
      ;
      ; upper half circel
      ;
      14: begin
          x=[1,.866, .707, .500, 0,-.500,-.707,-.866,-1,1]
          y=[0,.500, .707, .866, 1, .866, .707, .500, 0,0]
      end
      ;
      ; lower half circel
      ;
      15: begin
          x=[1,.866, .707, .500, 0,-.500,-.707,-.866,-1,1]
          y=[0,-.500,-.707,-.866,-1,-.866,-.707,-.500,0,0]
      end
      ;
      ; standing bar
      ;
      16: begin
          x=[-0.5,-0.5, 0.5, 0.5,-0.5]
          y=[-1, 1, 1,-1,-1]
      end
      ;
      ; laying bar
      ;
      17: begin
          x=[-1,-1, 1, 1,-1]
          y=[-0.5, 0.5, 0.5,-0.5,-0.5]
      end 
      ;
      ; hat up
      ;
      18: begin
          x=[-1, -0.5, -0.5, 0.5, 0.5, 1, -1]
          y=[-0.7, -0.7, 0.7, 0.7, -0.7, -0.7, -0.7]
      end 
      ;
      ; hat down
      ;
      19: begin
          x=[-1, -0.5, -0.5, 0.5, 0.5, 1, -1]
          y=[0.7, 0.7, -0.7, -0.7, 0.7, 0.7, 0.7]
      end 
      ;
      ; hat right
      ;
      20: begin
          x=[-0.7, -0.7, 0.7, 0.7, -0.7, -0.7, -0.7]
          y=[-1, -0.5, -0.5, 0.5, 0.5, 1, -1]
      end 
      ;
      ; hat left
      ;
      21: begin
          x=[0.7, 0.7, -0.7, -0.7, 0.7, 0.7, 0.7]
          y=[-1, -0.5, -0.5, 0.5, 0.5, 1, -1]
      end 
      ;
      ; big cross
      ;
      22: begin
          x=[1, 0.3, 0.3, -0.3, -0.3, -1, -1, -0.3, -0.3, 0.3, 0.3, 1, 1]
          y=[0.3, 0.3, 1, 1, 0.3, 0.3, -0.3, -0.3, -1, -1, -0.3, -0.3, 0.3]
      end 
      ;
      ; circle and plus
      ;
      23: begin
          x=[1,.866, .707, .500, 0,-.500,-.707,-.866,-1,$
             -.866,-.707,-.500, 0, .500, .707, .866, 1,$
             -1,   0,    0,    0]
          y=[0,.500, .707, .866, 1, .866, .707, .500, 0,$
             -.500,-.707,-.866,-1,-.866,-.707,-.500, 0,$
             0,   0,    1,   -1]
      end 
      ;
      ; circle and ex 
      ;
      24: begin
          x=[1,.866, .707, .500, 0,-.500,-.707,-.866,-1,$
             -.866,-.707,-.500, 0, .500, .707, .866, 1,$
             .866,.707,-.707,    0, .707,-.707]
          y=[0,.500, .707, .866, 1, .866, .707, .500, 0,$
             -.500,-.707,-.866,-1,-.866,-.707,-.500, 0,$
             .500,.707,-.707,    0,-.707, .707]
      end 
      ;
      ; right arrow 
      ;
      25: begin
          x=[ 0.000,0.000,0.342,0.000]
          y=[-1.000,1.000,0.000,0.000]
      end 
      ;
      ; left arrow 
      ;
      26: begin
          x=[ 0.000,0.000,-0.342,0.000]
          y=[-1.000,1.000, 0.000,0.000]
      end 
      ;
      ; full arrow 
      ;
      27: begin
          x=[ 0.000,0.000, 0.342,-0.342,0.000]
          y=[-1.000,1.000, 0.000,0.000,1.000]
      end 
      ;
      ; vertical line 
      ;
      28: begin
          x=[ 0.000,0.000]
          y=[-1.000,1.000]
      end 
      ;
      ; horizontal line
      ;
      29: begin
          x=[-1.000,1.000]
          y=[ 0.000,0.000]
      end
      ;
      ; x
      ;
      30: begin
          x=[-1, 0, 1, 0, 1, 0, -1, 0, -1]
          y=[1, 0, 1, 0, -1, 0, -1, 0, 1]
      end
      ;
      ; +
      ;
      31: begin
          x=[0,0,0,0,-1,0,1]
          y=[1,0,-1,0,0,0,0]
      end
      ; fall back is like sym=0
      else: begin
          x=[-1,-1, 1, 1,-1]
          y=[-1, 1, 1,-1,-1]
          x=x/10000.
          y=y/10000.
      end
  endcase
  ;
  usersym, x, y, fill=fill, thick=thick
  ;
  return
  ;
END
;-
