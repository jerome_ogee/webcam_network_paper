### SIMULATION FLAGS
year_simul    = 2010                -> in case multiyear dataset
constant_skyl = 0                   -> for variable diffuse light fraction
skyl0         = 25.                 -> diffuse light fraction (if constant_skyl=1)

### CAMERA SPECS (note: only NC and ADFC are possible for the moment) ###
camera_type         = NC            -> unitless
webcam_zenith_angle = 80.           -> degrees
webcam_azim_angle   = 0.            -> degrees
RGB_white_balance   = 255 168 185

### SITE COORDINATES ###
latitude     = 51.12                -> degrees
longitude180 = -0.85                -> degrees

### PHENOLOGICAL PARAMETERS ###
lai_max       = 3.5                 -> m2/m2
lai_min       = 0.                  -> m2/m2
bbd           = 150.                -> days since Jan 1st
lai_rate_up   = 0.16                -> day-1
senescence    = 320.                -> days since Jan 1st
lai_rate_down = -0.12               -> day-1

### CANOPY STRUCTURE PARAMETERS ###
leaf_angle        = 30.             -> degrees (Kull et al. 1999)
leaf_angle_at_bbd = 30.             -> degrees
#
LMA_max           = 0.008           -> g/cm2 (0.001-0.04) insensitive except NDVI (70-80g/m2 for Q. robur) (Demarez et al. 1999)
leaf_EWT          = 0.04            -> cm of water (0.001-0.15) insensitive
#
Ns_bbd            = 1.2             -> unitless (Demarez et al. 1999 suggests 1.0 for oak and beech)
Ns_min            = 1.2             -> unitless (Demarez et al. 1999 suggests 1.7 for oak and 1.4 for beech)
Ns_max            = 2.4             -> unitless (Demarez et al. 1999 suggests 2.4 for oak and 2.0 for beech)

### PIGMENT PHENOLOGICAL PARAMETERS ###
Cab_max        = 50.                -> microg/cm2 (0-100)
Cab_min        = 15.                -> microg/cm2
Cab_bbd        = 155.               -> days since Jan 1st
Cab_rate_up    = 0.12               -> day-1
Cab_senescence = 310.               -> days since Jan 1st
Cab_rate_down  = -0.18              -> day-1
#
Car_max        = 22.                -> microg/cm2 (0-40)
Car_min        =  2.                -> microg/cm2
Car_bbd        = 155.               -> days since Jan 1st
Car_rate_up    = 0.08               -> day-1
Car_senescence = 310.               -> days since Jan 1st
Car_rate_down  = -0.18              -> day-1
#
Cbrown_bbd     = 0.0                -> unitless (0.0001-8)
Cbrown_min     = 0.8 -> 0.0                -> unitless (0.0001-8)
Cbrown_max     = 4.5                -> unitless (0.0001-8)
