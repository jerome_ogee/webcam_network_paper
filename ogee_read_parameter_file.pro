FUNCTION OGEE_READ_PARAMETER_FILE, unit_in, skip=skip, array_flag=array_flag, type=type
  ;--- skip lines
  IF NOT(KEYWORD_SET(skip)) THEN skip=0
  str = ''
  FOR l=0,skip DO READF,unit_in,str
  ;--- read variable
  str = STRTRIM((STR_SEP(str,"->"))[0],0)
  IF NOT(KEYWORD_SET(type)) THEN type='string'
  IF NOT(KEYWORD_SET(array_flag)) THEN array_flag=0
  CASE type OF
     'integer': BEGIN
        IF NOT(array_flag) THEN $
           res = FIX((STR_SEP(str," = "))[1]) $
        ELSE $
           res = FIX(STR_SEP(((STR_SEP(str," = "))[1]),' '))
     END
     'long': BEGIN
        IF NOT(array_flag) THEN $
           res = LONG((STR_SEP(str," = "))[1]) $
        ELSE $
           res = LONG(STR_SEP(((STR_SEP(str," = "))[1]),' '))
     END
     'double': BEGIN
        IF NOT(array_flag) THEN $
           res = DOUBLE((STR_SEP(str," = "))[1]) $
        ELSE $
           res = DOUBLE(STR_SEP(((STR_SEP(str," = "))[1]),' '))
     END
     'real': BEGIN
        IF NOT(array_flag) THEN $
           res = FLOAT((STR_SEP(str," = "))[1]) $
        ELSE $
           res = FLOAT(STR_SEP(((STR_SEP(str," = "))[1]),' '))
     END
     'string': BEGIN
        IF NOT(array_flag) THEN $
           res = (STR_SEP(str," = "))[1] $
        ELSE $
           res = STR_SEP(((STR_SEP(str," = "))[1]),' ')
     END
  ENDCASE
RETURN,res
END
