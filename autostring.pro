;+
; NAME:
;	AUTOSTRING
;
; PURPOSE:
;       Returns a string containing the value of a number
;         with the specified number of decimal (default = 0)
;
; CATEGORY:
;	General.
;
; CALLING SEQUENCE:
;	result = autostring(array, decimals, /zero)
;
; INPUT:
;       array:  array of (integer or real) numbers
;
; OUTPUT:
;	result: string array of formatted numbers
;
; OPTIONAL INPUT:
;	decimals: number of decimal places of formatted values
;
; KEYWORDS:
;	zero:     if non-zero, pad values with zeros rather than blanks
;
; OPTIONAL KEYWORDS:
;	None.
;
; INPUT/OUTPUT:
;	None.
;
; INTERNAL FUNCTIONS, PROCEDURES, and FILES:
;       function autoformat - Returns format code
;
; EXTERNAL FUNCTIONS, PROCEDURES, and FILES:
;       None.
;
; NAMED STRUCTURES:
;       None.
;
; COMMON BLOCKS:
;       None.
;
; PROCEDURE:
;	IDL> s_array = autostring(array,2)
;
; MODIFICATION HISTORY:
;	Written, unknown
;       Modified, MC, Feb. 2008 - zero keyword
;                               - no external routine autoformat
;-
FUNCTION autostring, number, num_decimal_places, zero=zero
  COMPILE_OPT idl2
  ON_ERROR, 2
  ;
  if n_elements(num_decimal_places) eq 0 then numdec = 0 else numdec = num_decimal_places
  ; Pad with zeros
  if keyword_set(zero) then nix = '0' else nix = ''
  ;
  ;--- special treatment of -0.0, should work for all types
  ii = where(number eq 0, count)
  if count gt 0 then number[ii] = 0
  ;
  ; Determine what we're dealing with
  ; IDL's definition of type_code
  ; 0  Undefined
  ; 1  Byte
  ; 2  Integer
  ; 3  Longword integer
  ; 4  Floating-point
  ; 5  Double-precision floating
  ; 6  Complex floating
  ; 7  String
  ; 8  Structure
  ; 9  Double-precision complex
  ;10  Pointer
  ;11  Object reference
  ;12  Unsigned Integer
  ;13  Unsigned Longword Integer
  ;14  64-bit Integer
  ;15  Unsigned 64-bit Integer
  type_code = size(number, /type)
  ; Do not process strings
  if type_code eq 7 then begin
      return, number
  endif
  ;
  ; Determine format code
  ;
  abs_num = abs(number)
  ;--- if we deal with an array of numbers we take the largest....
  abs_num = max([abs_num])
  ;
  ; leave room for the decimal point and the negative sign, if any
  if min([number]) lt 0. then num_sign_chars = 1 else num_sign_chars = 0
  ;
  ; Floating point
  if ((type_code eq 4) or (type_code eq 5)) then begin
      ; pre-assign the format_type just in case number falls through if statements
      format_type = 'f'
      ; number is a float, more or less
      if abs_num ge 1.e6 then begin
          num_prefix_chars  = 1
          num_sci_not_chars = 4
          format_type       = 'g'
      endif else if ((abs_num lt 1.e6) and (abs_num ge 1.)) then begin
          nprefix = fix(alog10(long(abs_num)))+1
          ; special treatment: the output prefix digits could
          ; be one digit longer as the input prefix digits: e.g. 99.99 => 100.0
          val               = round(abs_num*10.^numdec,/l64)/10.^numdec
          nprefixval        = fix(alog10(val))+1
          nprefix           = max([nprefix,nprefixval])
          num_prefix_chars  = nprefix
          num_sci_not_chars = 0
          format_type       = 'f'
      endif else if ((abs_num lt 1.) and (abs_num ge 1.e-3)) then begin
          ; unfortunately, it seems impossible to get rid of leading zeroes before formatting
          num_prefix_chars  = 1
          num_sci_not_chars = 0
          format_type       = 'f'
      endif else if (abs_num eq 0.) then begin
          num_prefix_chars  = 1
          num_sci_not_chars = 0
          format_type       = 'f'
      endif else begin 
          num_prefix_chars  = 1
          num_sci_not_chars = 4
          format_type       = 'g'
      endelse
      ;
      if format_type eq 'e' then $
        num_postfix_chars = numdec $
      else if format_type eq 'f' then $
        num_postfix_chars = numdec $
      else if format_type eq 'g' then $
        num_postfix_chars = numdec
      ;
      num_total_chars = num_sign_chars + num_prefix_chars + 1 $
                        + numdec + num_sci_not_chars
      ;
      if num_total_chars lt 10 then $
        format_string = string(format='("(",A1,A,I1,".",I1,")")', $
                               format_type, nix, num_total_chars, num_postfix_chars) $
      else $
        if num_postfix_chars lt 10 then $
          format_string = string(format='("(",A1,A,I2,".",I1,")")', $
                                 format_type, nix, num_total_chars, num_postfix_chars) $
        else $
          format_string = string(format='("(",A1,A,I2,".",I2,")")', $
                                 format_type, nix, num_total_chars, num_postfix_chars)
  ;
  endif else if ((type_code eq 1) or (type_code eq 2) or (type_code eq 3) $
                 or (type_code eq 12) or (type_code eq 13) $
                 or (type_code eq 14) or (type_code eq 15)) then begin
      ; number is an integer
      format_type = 'i'
      if abs_num ne 0 then $
        num_digits = fix(alog10(abs_num))+1 $
      else $
	num_digits = 1 
      ;
      num_total_chars = num_digits+num_sign_chars
      ;
      if num_total_chars lt 10 then $
        format_string = string(format='("(",A1,A,I1,")")', $
                               format_type, nix, num_total_chars) $
      else $
        format_string = string(format='("(",A1,A,I2,")")', $
                               format_type, nix, num_total_chars)
  ;
  endif else begin              ; end if number is integer
      ; number is currently unformattable
      message, 'Number code not supported'
  endelse                       ;
  ;
  ; return formatted string
  return, string(format=format_string, number)
  ;
end
;-
