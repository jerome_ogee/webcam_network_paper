;+
; NAME:
;	MC_LLEGEND
;
; PURPOSE:
;	Create a line and text legend 
;	and place it on the plotting 
;	surface.
;	MODIFICATION OF CCG_LLEGEND TO BE ABBLE TO DEFINE THE 
;	POSITION OF THE LEGEND IN DATA COORDINATES.... ELSE 
;	WORK LIKE CCG_LLEGEND.... AND ALSO TO INCLUDE SYMBOL
;	IN THE LINE PLOTTING (using mc_symbol definitions)...
;
;	Type 'legend_ex' for a legend example.
;
; CATEGORY:
;	Graphics.
;
; CALLING SEQUENCE:
;	MC_LLEGEND,x=x,y=y,tarr=tarr,larr=larr,carr=carr,
;		     /data
;
; INPUTS:
;	x: y:	upper left corner of legend.
;		Specify in NORMAL coordinates	
;		i.e., bottom/left of plotting surface -> x=0,y=0 
;		      top/right of plotting surface   -> x=1,y=1 
;		IF THE KEYWORD DATA IS SPECIFIED THEN X AND Y
;		CORRESPOND TO DATA COORDINATES...
;
;	tarr:  	text vector
;	larr:	line type vector (see IDL for LINESTYLE), -1 is no line
;	ltarr:	thickness of lines
;	carr:	color vector (values [0-255])
;	sarr:   symbol array : to add on the line plot 
;		according to mc_symbol; 0: no symbol
;	farr:   1/0 for fill/unfill of the symbols sarr
;               >=2 one filled and one unfilled is drawn
;
;	NOTE:	All vectors must be the same length
;
; OPTIONAL INPUT PARAMETERS:
;	charsize:	text size (default: 1)
;	charthick:	text thickness (default: 1)
;	thick:		line thickness if not ltarr is given (default: 1)
;       symthick:       symbol thickness (default: 1)
;	frame:		If set to 1 - draw legend frame
;			If set to 0 - no legend frame	
;	data:           keyword : if set then data coord.
;       llength:        Length of lines (default: 2)
;       space:          Space between columns (default: 0)
;
; OUTPUTS:
;	None.
;
; COMMON BLOCKS:
;	None.
;
; SIDE EFFECTS:
;	None.
;
; RESTRICTIONS:
;	All vectors must be the same length
;
; PROCEDURE:
;	
;	Example:
;
;		PRO 	example,dev=dev
;		;
;		OPENDEVICE,dev=dev,pen=pen
;		.
;		.
;		.
;		;
;		;draw initial plot.
;		;
;		PLOT,	[0,0],[1,1]
;		;
;		;define vectors to be passed to MC_LLEGEND.
;		;
;		tarr=[	'BRW','MLO','SMO','SPO']
;
;		larr=[1,2,3,4]
;
;		carr=[	pen(2),pen(3),pen(4),pen(5)]
;		;
;		;call to MC_LLEGEND
;		;
;		MC_LLEGEND,	x=.2,y=.8,$
;				tarr=tarr,$
;				larr=larr,$
;				carr=carr,$
;				charthick=2.0,$
;				thick=5.0,$
;				charsize=2.0,$
;				frame=1
;		.
;		.
;		.
;		CLOSEDEVICE,dev=dev
;		END
;
; MODIFICATION HISTORY:
;	Written, KAM, February 1994.
;       Modified, PEYL
;       Modified, MC, February 2002, if no line then only one symbol
;       (not two).
;                 MC, Dec. 2003 - nothing to adapt for Windows
;                 MC, April 2005 - win work around
;-
PRO    	MC_LLEGEND,	x=x,y=y,$
			tarr=tarr,larr=larr,carr=carr,ltarr=ltarr,$
			charsize=charsize,$
			charthick=charthick,$
			thick=thick,$
			frame=frame,$
			sarr=sarr,$
			farr=farr,$
                        incr_text=incr_text,$
			data=data $
                      , symthick=symthick $
                      , symsize=symsize $
                      , llength=llength $
                      , font=font $
                      , col=col $
                      , row=row $
                      , vertical=vertical $
                      , space=space $
                      , cspace=cspace $
                      , lspace=lspace
;
;return to caller if an error occurs
ON_ERROR,2
;
;determine number of lines
n=N_ELEMENTS(tarr)
;
;If keywords are not set then assign default values
; size allows keyword=0; from ccg_vdef
IF size(thick,/type) eq 0 THEN thick=1.
IF size(symthick,/type) eq 0 THEN sthick=1. else sthick=symthick
IF size(symsize,/type) eq 0 THEN ssize=1. else ssize=symsize
IF size(charthick,/type) eq 0 THEN charthick=1.
IF size(charsize,/type) eq 0 THEN charsize=1.
IF size(carr,/type) eq 0 THEN carr=replicate(0,n)
IF size(larr,/type) eq 0 THEN larr=replicate(-1,n)
IF size(ltarr,/type) eq 0 THEN ltarr=replicate(thick,n)
; Windows workaround for 'WIN'
if !D.name eq 'WIN' then ltarr1=float(ltarr)<3.4 else ltarr1=float(ltarr)
IF size(sarr,/type) eq 0 THEN sarr=replicate(0,n)
IF size(farr,/type) eq 0 THEN farr=replicate(0,n)
IF size(x,/type) eq 0 THEN x=0.
IF size(y,/type) eq 0 THEN y=0.
IF size(data,/type) eq 0 THEN data=0
xx=x
yy=y
if data then begin
    s=!X.S
    xx = s[1]*x + s[0]
    s=!Y.S
    yy = s[1]*y + s[0]
    data=0
endif
IF size(incr_text,/type) eq 0 THEN incr_text=1.
IF size(llength,/type) eq 0 THEN llength=2.
IF size(font,/type) eq 0 THEN font=-1
IF size(row,/type) ne 0 THEN BEGIN
    IF size(col,/type) eq 0 THEN col=fix((n-1)/row)+1
ENDIF ELSE BEGIN
    IF size(col,/type) eq 0 THEN BEGIN
        col=1
        row=n
    ENDIF ELSE row=fix((n-1)/col)+1
ENDELSE
IF size(vertical,/type) eq 0 THEN vert=0 else vert=1
IF size(cspace,/type) eq 0 THEN cspace=1.
IF size(lspace,/type) eq 0 THEN lspace=1.
; for old code: space=cspace
IF size(space,/type) ne 0 THEN cspace=space
;
if col*row lt n then begin
    print,' '
    print,'ERROR MC_LLEGEND: Col*Row < n_elements(tarr).',string(7B)
    return
endif    
;
;determine length of longest text
; note: we remove font codes starting with '!' and a number...
;
;tlen=MAX(STRLEN(tarr))
tarr2 = tarr
for i=0,n_elements(tarr2)-1 do begin
   j = STRPOS(tarr2[i],'!')
   while j GE 0 do begin
      prefix = STRMID(tarr2[i], 0, j)
      suffix = STRMID(tarr2[i], j+2, STRLEN(tarr2[i]) - (j+2))
      tarr2[i] = prefix + suffix
      j = STRPOS(tarr2[i],'!')
   endwhile
endfor
tlen=MAX(STRLEN(tarr2))

;
; increment for text
;
pscsize=10.
pswidth=600.
psheight=800.
pscwidth=pscsize/pswidth
pscheight=pscsize/psheight
linelength=llength*pscwidth*charsize
spaci=pscwidth*charsize
xinc=linelength+spaci*lspace+tlen/2.*pscwidth*charsize+spaci*cspace
yinc=2.*pscheight*charsize*incr_text
;
FOR i=0,n-1 DO BEGIN
    
    if sarr[i] ne 0 then $
      if sarr[i] lt 0 then $
        psym = -8 $
      else $
        psym = 8 $
    else $
      psym = 0

    if vert then begin
        y1 = i mod row
        x1 = fix(i / row) mod col
    endif else begin
        x1 = i mod col
        y1 = fix(i / col) mod row
    endelse
    
    if psym eq 8 then begin
        if farr[i] ge 2 then begin
;           1 Symbol filled and 1 Symbol unfilled
            mc_symbol, sym=abs(sarr[i]), fill=1, thick=sthick
            PLOTS, (xx+(xinc*x1)-linelength/4.) $
              , [yy-(yinc*y1)] $
              , LINESTYLE=larr[i] $
              , THICK=ltarr1[i] $
              , COLOR=carr[i] $
              , PSYM=psym $
              , symsize=ssize $
              , /NORMAL
            mc_symbol, sym=abs(sarr[i]), fill=0, thick=sthick
            PLOTS, (xx+(xinc*x1)+linelength/4.) $
              , [yy-(yinc*y1)] $
              , LINESTYLE=larr[i] $
              , THICK=ltarr1[i] $
              , COLOR=carr[i] $
              , PSYM=psym $
              , symsize=ssize $
              , /NORMAL
        endif else begin
;           1 Symbol unfilled or filled            
            mc_symbol, sym=abs(sarr[i]), fill=min([farr[i],1]), thick=sthick
            PLOTS, (xx+(xinc*x1)) $
              , [yy-(yinc*y1)] $
              , LINESTYLE=larr[i] $
              , THICK=ltarr1[i] $
              , COLOR=carr[i] $
              , PSYM=psym $
              , symsize=ssize $
              , /NORMAL
        endelse
    endif else begin
        if psym ne 0 then begin
            if farr[i] ge 2 then begin
;               1 Symbol filled and 1 Symbol unfilled
                mc_symbol, sym=abs(sarr[i]), fill=1, thick=sthick
                PLOTS, (xx+(xinc*x1)-linelength/4.) $
                  , [yy-(yinc*y1)] $
                  , LINESTYLE=larr[i] $
                  , THICK=ltarr1[i] $
                  , COLOR=carr[i] $
                  , PSYM=psym $
                  , symsize=ssize $
                  , /NORMAL
                mc_symbol, sym=abs(sarr[i]), fill=0, thick=sthick
                PLOTS, (xx+(xinc*x1)+linelength/4.) $
                  , [yy-(yinc*y1)] $
                  , LINESTYLE=larr[i] $
                  , THICK=ltarr1[i] $
                  , COLOR=carr[i] $
                  , PSYM=psym $
                  , symsize=ssize $
                  , /NORMAL
            endif else begin
;               1 Symbol unfilled or filled            
                mc_symbol, sym=abs(sarr[i]), fill=min([farr[i],1]), thick=sthick
                PLOTS, (xx+(xinc*x1)) $
                  , [yy-(yinc*y1)] $
                  , LINESTYLE=larr[i] $
                  , THICK=ltarr1[i] $
                  , COLOR=carr[i] $
                  , PSYM=psym $
                  , symsize=ssize $
                  , /NORMAL
            endelse
        endif
;       Line
        if larr[i] ge 0 then $
          PLOTS, [(xx+(xinc*x1))-linelength/2.,(xx+(xinc*x1))+linelength/2.] $
          , [yy-(yinc*y1),yy-(yinc*y1)] $
          , LINESTYLE=larr[i] $
          , THICK=ltarr1[i] $
          , COLOR=carr[i] $
          , PSYM=0 $
          , /NORMAL
    endelse
;       Text: in postscript, charsize 1.0 has about 10 points (of 800)
;             per character height = 1/80=0.0125
;             xyouts aligns text basline, therefore offset half a
;             character height
    XYOUTS, (xx+(xinc*x1))+linelength/2.+spaci*lspace,yy-(yinc*y1)-pscheight/2.*charsize,$
      /NORMAL,$
      tarr[i],$
      ALI=0,$
      CHARSIZE=charsize,$
      CHARTHICK=charthick,$
      COLOR=carr[i],$
      font=font
        
ENDFOR
;
;build legend frame --- not working
;
;IF size(frame,/type) ne 0 THEN BEGIN
;	PLOTS,	xx-(.02*charsize),$
;		yy+(.03*charsize),$
;		/NORMAL
;	PLOTS,	xx-(.02*charsize),$
;		yy-(n*.025*charsize*incr_text),$
;		/NORMAL,$
;		/CONTINUE
;	PLOTS,	xx+(charsize*.04)+(charsize*(tlen+2)*.0095)+(charsize*.012),$
;		yy-(n*.025*charsize*incr_text),$
;		/NORMAL,$
;		/CONTINUE
;	PLOTS,	xx+(charsize*.04)+(charsize*(tlen+2)*.0095)+(charsize*.012),$
;		yy+(.03*charsize),$
;		/NORMAL,$
;		/CONTINUE
;	PLOTS,	xx-(.02*charsize),$
;		yy+(.03*charsize),$
;		/NORMAL,$
;		/CONTINUE
;ENDIF
IF size(frame,/type) ne 0 THEN BEGIN
	PLOTS,	xx-(.015*charsize),$
		yy+(.02*charsize),$
		/NORMAL
	PLOTS,	xx-(.015*charsize),$
		yy-(n*.025*charsize*incr_text),$
		/NORMAL,$
		/CONTINUE
	PLOTS,	xx+(charsize*.015)+(charsize*(tlen)*.0095)+(charsize*.012),$
		yy-(n*.025*charsize*incr_text),$
		/NORMAL,$
		/CONTINUE
	PLOTS,	xx+(charsize*.015)+(charsize*(tlen)*.0095)+(charsize*.012),$
		yy+(.02*charsize),$
		/NORMAL,$
		/CONTINUE
	PLOTS,	xx-(.015*charsize),$
		yy+(.02*charsize),$
		/NORMAL,$
		/CONTINUE
ENDIF
;
END
;-



