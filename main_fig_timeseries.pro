;+
; DESCRIPTION:
; This program simulates webcam RGB signals using a BRDF model
;
; Canopy directional reflectance modelling is based on PROSAIL:
; - modeling leaf optical properties with PROSPECT-5 (Feret et al. 2008)
; - modeling leaf inclination distribution function using Campbell's
;   ellipsoidal distribution function caracterised by the average leaf 
;   inclination angle in degree
; - modeling canopy reflectance with 4SAIL (Verhoef et al., 2007)
; - modeling incoming solar spectra for direct and diffuse light based
;   on 6S simulations from Francois et al. (2002)
; 
; REFERENCES:
; Verhoef et al. (2007) Unified Optical-Thermal Four-Stream Radiative
;   Transfer Theory for Homogeneous Vegetation Canopies, IEEE TRANSACTIONS 
;  ON GEOSCIENCE AND REMOTE SENSING, 45(6).
; Féret et al. (2008), PROSPECT-4 and 5: Advances in the Leaf Optical
;   Properties Model Separating Photosynthetic Pigments, REMOTE SENSING OF 
;  ENVIRONMENT.
; Francois et al. (2002) Conversion of 400–1100 nm vegetation albedo 
;    measurements into total shortwave broadband albedo using a canopy 
;    radiative transfer model. AGRONOMY, 22.

; NAME: main
;
; AUTHOR/HISTORY:
; Original Matlab code written by Jean-Baptiste Féret (feret@ipgp.fr)
;    Institut de Physique du Globe de Paris, Space and Planetary Geophysics
;    during October 2009 based on a version of PROSAIL provided by
;    Wout Verhoef, NLR on April/May 2003.
; Translated from Matlab to IDL by Eben N. Broadbent (ebennb@gmail.com)
;    Dept. of Biology, Stanford University.
; Adapted for Webcam studies by Jerome Ogee (jogee@bordeaux.inra.fr)
;    INRA, UR 1263, 33140 Villenave d'Ornon, France.
;
; CONTACT INFO: jogee@bordeaux.inra.fr
;
; CALLING SEQUENCE:
;       main, simul_id=simul_id, write_ascii=write_ascii
;
; INPUTS: none, parameters are set within the procedure
;
; OUTPUTS: none (only plots)
;
; OPTIONAL INPUT(S): simul_id (id of webcam data file)
;
; OPTIONAL OUTPUT(S): none
;
; OPTIONAL INPUT KEYWORD(S): write_ascii (to write output files in ASCII)
;
; NOTES:
; The specific absorption coefficient corresponding to brown pigment is
;   provided by Frederic Baret (EMMAH, INRA Avignon, baret@avignon.inra.fr)
;   and used with his autorization.
; The model PRO4SAIL is based on a version provided by Wout Verhoef (NLR)
;   in April/May 2003
;
; METHOD:
;
; EXAMPLE:
;
; MODIFICATION HISTORY:
; 09/10/2010 Downloaded from "http://teledetection.ipgp.jussieu.fr/prosail/"
; 09/27/2010 Translated from matlab to IDL by Eben N. Broadbent
; March 2012 Adapated for Webcam studies by Jerome Ogee
;
; CODING NOTES:
; Same as downloaded Matlab version
; Some more comments added on IDL version by J. Ogee
;-

pro main, simul_id=simul_id, write_ascii=write_ascii, $
          constant_skyl=constant_skyl

@ogee_general_header.inc

;--- physical constants
h_planck = 6.62606957d-34       ;--- J.s
c_light = 299792458d0           ;--- m/s
n_avogadro = 6.023e23           ;--- mol-1

;--- possible flag
IF NOT(KEYWORD_SET(simul_id)) THEN simul_id = 'UKHam_NC_2010' ;'UKHam_ADFC_2010'
IF N_ELEMENTS(STR_SEP(simul_id,'_')) GT 1 THEN site_id = (STR_SEP(simul_id,'_'))[0] $
ELSE site_id = simul_id
IF NOT(KEYWORD_SET(write_ascii)) THEN write_ascii = 0
IF write_ascii THEN debug = 0 ELSE debug = 1
debug = 0
;IF NOT(KEYWORD_SET(constant_skyl)) THEN constant_skyl = 1

;--- re-adjust folders
SPAWN,wdir,path0
path_data_local = path0 + dl + 'io_files' + dl ;+ site_id + dl
path_out        = path0 + dl + 'io_files' + dl ;+ site_id + dl
;path_data_local = GETENV(wdir) + dl + 'io_files' + dl ;+ site_id + dl
;path_out        = GETENV(wdir) + dl + 'io_files' + dl ;+ site_id + dl

;--- store default color tables to reset whenever we want to
;CCG_RGBLOAD,file=!DIR+'/lib/My_IDL/ccg_lib/'+'data/color_comb1'
CCG_RGBLOAD,file='./ccg_color_comb1'
TVLCT,red,green,blue,/GET

;--- spec of the Sun
plot_solar_spectrum, path_for_plot=path_out, $
                     skyl=40., Rg=800.

;--- open parameter file
OPENR,unit_in,path_data_local + simul_id + '.in',/GET_LUN

;--- general flags
year_simul    = OGEE_READ_PARAMETER_FILE(unit_in, skip=1, array_flag=0, type='integer')
;constant_skyl = OGEE_READ_PARAMETER_FILE(unit_in, skip=0, array_flag=0, type='integer')
;skyl0         = OGEE_READ_PARAMETER_FILE(unit_in, skip=0, array_flag=0, type='double' )
IF constant_skyl THEN BEGIN
   skip = 2
   skyl0         = OGEE_READ_PARAMETER_FILE(unit_in, skip=1, array_flag=0, type='double' )
ENDIF ELSE BEGIN
   skip = 4
   skyl0 = 'variable'
ENDELSE
IF debug THEN BEGIN
   PRINT, ''
   PRINT, '--- SIMULATION FLAGS ---'
   PRINT, 'site_id             = ', site_id
   PRINT, 'year_simul          = ', year_simul
   PRINT, 'constant_skyl       = ', constant_skyl
   PRINT, 'skyl0               = ', skyl0
ENDIF

;-----------------------
;--- specs of the camera
;-----------------------
camera_type         = OGEE_READ_PARAMETER_FILE(unit_in, skip=skip, array_flag=0, type='string')
webcam_zenith_angle = OGEE_READ_PARAMETER_FILE(unit_in, skip=0, array_flag=0, type='double')
webcam_azim_angle   = OGEE_READ_PARAMETER_FILE(unit_in, skip=0, array_flag=0, type='double')
RGB_white_balance   = OGEE_READ_PARAMETER_FILE(unit_in, skip=0, array_flag=1, type='integer')
IF debug THEN BEGIN
   PRINT, ''
   PRINT, '--- CAMERA SPECS ---'
   PRINT, 'camera_type         = ', camera_type
   PRINT, 'webcam_zenith_angle = ', webcam_zenith_angle, ' degrees'
   PRINT, 'webcam_azim_angle   = ', webcam_azim_angle, ' degrees'
   PRINT, 'RGB_white_balance   = ', RGB_white_balance
ENDIF
;--- note: we add an extra wavelength at 300nm and 760nm to insure the
;    interpolation does not go crazy for l<390nm and l>750nm...
;    BUT below 400nm we do not have resv or I_tot values anyway...
CASE camera_type OF
   'NC': BEGIN
      l_quantum_yield     = [380, 390,400,425,450,475,500,525,530,550,575,600,625,650,675,700,725,750,760]
      quantum_yield_red   = [  0, 6,  5,  2,1.5,  2,3.5,7.5,7.5,  7, 10, 37, 30, 27, 20, 19, 19, 20, 20]
      quantum_yield_green = [  0, 5,  5,5.5,  9, 20, 30, 45, 46, 40, 25, 13,  7,  6,7.5, 10, 10, 12, 12]
      quantum_yield_blue  = [ 0, 17, 26, 37, 43, 36, 20, 10,  9,  7,  5,4.5,  4,  4,  4,  5,  5,  6,  6]
      ;--- from last email from Daniel Lawton and Anthony Watts
      ;    IR cutoff=650nm and UV cutoff is below 400nm
      l_IR_filter = 650.
      l_UV_filter = 380.
   END
   'ADFC': BEGIN
      l_quantum_yield     = [400,425,440,450,465,475,500,512,525,540,575,590,610,625,650,670,685,700]
      quantum_yield_red   = [  0, 14, 18, 17, 13,  6,  0,  0,  0,  0, 42, 77, 86, 70, 32,  9,  0,  0]
      quantum_yield_green = [  0, 11, 10, 12, 23, 42, 77, 91,100, 89, 42,  9,  0,  0,  0,  0,  0,  0]
      quantum_yield_blue  = [  0, 40, 70, 80, 83, 80, 40, 20,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0]
      l_IR_filter = 685.
      l_UV_filter = 380. ;???
   END
   ELSE: BEGIN
      PRINT,'Type of camera not known. Only NC- and ADFC-type are possible'
      STOP
   END
ENDCASE
;---
quantum_yield = [ [quantum_yield_red   ] ,$
                  [quantum_yield_green ] ,$
                  [quantum_yield_blue  ] ]

;--- convert into normalised values
FOR ic=0,2 DO BEGIN
   ii = where(l_quantum_yield le l_UV_filter OR l_quantum_yield GE l_IR_filter,nii)
   IF nii GT 0 THEN quantum_yield[ii,ic] = 0.
   IF camera_type EQ 'NC' THEN quantum_yield[*,ic] = quantum_yield[*,ic] * (l_quantum_yield/1e3)
ENDFOR
factor =  100. / MAX(quantum_yield)
quantum_yield = quantum_yield * factor

;--- color scheme
n_RGB = 3
RGB_color_TAB =  [2, 63, 23]
RGB_color_TAB_mod = [53,4,22]
;plot_webcam_specs, path_for_plot=path_out, $
;                  camera_type=camera_type, $
;                  RGB_color_tab=RGB_color_tab,$
;                  l_UV_filter=l_UV_filter,$
;                  l_IR_filter=l_IR_filter,$
;                  l_quantum_yield=l_quantum_yield,$
;                  quantum_yield=quantum_yield

;-----------------
;--- spec of the site
;-----------------

;--- location and time
latitude       = OGEE_READ_PARAMETER_FILE(unit_in, skip=2, array_flag=0, type='double')
longitude180   = OGEE_READ_PARAMETER_FILE(unit_in, skip=0, array_flag=0, type='double')
;time           = OGEE_READ_PARAMETER_FILE(unit_in, skip=0, array_flag=0, type='double')
IF debug THEN BEGIN
   PRINT, ''
   PRINT, '--- SITE COORDINATES ---'
   PRINT, 'latitude            = ', latitude, ' degrees'
   PRINT, 'longitude180        = ', longitude180, ' degrees'
   ;PRINT, 'time                = ', time, ' h'
ENDIF

;--- lai
lai_max       = OGEE_READ_PARAMETER_FILE(unit_in, skip=2, array_flag=0, type='double') ;--- m2/m2
lai_min       = OGEE_READ_PARAMETER_FILE(unit_in, skip=0, array_flag=0, type='double') ;--- m2/m2
bbd           = OGEE_READ_PARAMETER_FILE(unit_in, skip=0, array_flag=0, type='double') ;--- days, since Jan 1st
lai_rate_up   = OGEE_READ_PARAMETER_FILE(unit_in, skip=0, array_flag=0, type='double') ;--- day-1
senescence    = OGEE_READ_PARAMETER_FILE(unit_in, skip=0, array_flag=0, type='double') ;--- days, since Jan 1st
lai_rate_down = OGEE_READ_PARAMETER_FILE(unit_in, skip=0, array_flag=0, type='double') ;--- day-1
IF debug THEN BEGIN
   PRINT, ''
   PRINT, '--- PHENOLOGY PARAMETERS ---'
   PRINT, 'lai_max             = ', lai_max, ' m2/m2'
   PRINT, 'lai_min             = ', lai_min, ' m2/m2'
   PRINT, 'bbd                 = ', bbd, ' days since Jan 1st'
   PRINT, 'lai_rate_up         = ', lai_rate_up, ' day-1'
   PRINT, 'senescence          = ', senescence, ' days since Jan 1st'
   PRINT, 'lai_rate_down       = ', lai_rate_down, ' day-1'
ENDIF

;--- leaf angle, Cm (or LMA), Cw (or EWT) and Ns
leaf_angle        = OGEE_READ_PARAMETER_FILE(unit_in, skip=2, array_flag=0, type='double') ;--- average leaf (zenithal?) angle (°)
leaf_angle_at_bbd = OGEE_READ_PARAMETER_FILE(unit_in, skip=0, array_flag=0, type='double')
;---
Cm_max            = OGEE_READ_PARAMETER_FILE(unit_in, skip=1, array_flag=0, type='double') ;--- g/cm2
Cw_0              = OGEE_READ_PARAMETER_FILE(unit_in, skip=0, array_flag=0, type='double') ;--- cm of water
;---
Ns_bbd            = OGEE_READ_PARAMETER_FILE(unit_in, skip=1, array_flag=0, type='double') ;--- min. structural parameter
Ns_min            = OGEE_READ_PARAMETER_FILE(unit_in, skip=0, array_flag=0, type='double') ;--- min. structural parameter (quite sensitive!)
Ns_max            = OGEE_READ_PARAMETER_FILE(unit_in, skip=0, array_flag=0, type='double') ;--- max. structural parameter (quite sensitive!)
IF debug THEN BEGIN
   PRINT, ''
   PRINT, '--- CANOPY STRUCTURE PARAMETERS ---'
   PRINT, 'leaf angle          = ', leaf_angle, ' degrees'
   PRINT, 'leaf angle at bbd   = ', leaf_angle_at_bbd, ' degrees'
   PRINT, 'maximum LMA (or Cm) = ', Cm_max, ' g/cm2'
   PRINT, 'leaf_EWT (or Cw)    = ', Cw_0, ' cm of water'
   PRINT, 'Ns_bbd              = ', Ns_bbd, ' -'
   PRINT, 'Ns_min              = ', Ns_min, ' -'
   PRINT, 'Ns_max              = ', Ns_max, ' -'
ENDIF

;--- Cab and Car
Cab_max        = OGEE_READ_PARAMETER_FILE(unit_in, skip=2, array_flag=0, type='double') ;--- microg/cm2
Cab_min        = OGEE_READ_PARAMETER_FILE(unit_in, skip=0, array_flag=0, type='double') ;--- microg/cm2
Cab_bbd        = OGEE_READ_PARAMETER_FILE(unit_in, skip=0, array_flag=0, type='double') ;--- days, since Jan 1st
Cab_rate_up    = OGEE_READ_PARAMETER_FILE(unit_in, skip=0, array_flag=0, type='double') ;--- day-1
Cab_senescence = OGEE_READ_PARAMETER_FILE(unit_in, skip=0, array_flag=0, type='double') ;--- days, since Jan 1st
Cab_rate_down  = OGEE_READ_PARAMETER_FILE(unit_in, skip=0, array_flag=0, type='double') ;--- day-1
;---
Car_max        = OGEE_READ_PARAMETER_FILE(unit_in, skip=1, array_flag=0, type='double') ;--- microg/cm2
Car_min        = OGEE_READ_PARAMETER_FILE(unit_in, skip=0, array_flag=0, type='double') ;--- microg/cm2
Car_bbd        = OGEE_READ_PARAMETER_FILE(unit_in, skip=0, array_flag=0, type='double') ;--- days, since Jan 1st
Car_rate_up    = OGEE_READ_PARAMETER_FILE(unit_in, skip=0, array_flag=0, type='double') ;--- day-1
Car_senescence = OGEE_READ_PARAMETER_FILE(unit_in, skip=0, array_flag=0, type='double') ;--- days, since Jan 1st
Car_rate_down  = OGEE_READ_PARAMETER_FILE(unit_in, skip=0, array_flag=0, type='double') ;--- day-1
Car_lag_max = 0.                                                          ;--- days
;---
Cbrown_bbd     = OGEE_READ_PARAMETER_FILE(unit_in, skip=1, array_flag=0, type='double') ;--- unitless
Cbrown_min     = OGEE_READ_PARAMETER_FILE(unit_in, skip=0, array_flag=0, type='double') ;--- unitless
Cbrown_max     = OGEE_READ_PARAMETER_FILE(unit_in, skip=0, array_flag=0, type='double') ;--- unitless
IF debug THEN BEGIN
   PRINT, ''
   PRINT, '--- PIGMENT PHENOLOGY PARAMETERS ---'
   PRINT, 'Cab_max             = ', Cab_max, ' microg/cm2'
   PRINT, 'Cab_min             = ', Cab_min, ' microg/cm2'
   PRINT, 'Cab_bbd             = ', Cab_bbd, ' days since Jan 1st'
   PRINT, 'Cab_rate_up         = ', Cab_rate_up, ' day-1'
   PRINT, 'Cab_senescence      = ', Cab_senescence, ' days since Jan 1st'
   PRINT, 'Cab_rate_down       = ', Cab_rate_down, ' day-1'
   PRINT, 'Car_max             = ', Car_max, ' microg/cm2'
   PRINT, 'Car_min             = ', Car_min, ' microg/cm2'
   PRINT, 'Car_bbd             = ', Car_bbd, ' days since Jan 1st'
   PRINT, 'Car_rate_up         = ', Car_rate_up, ' day-1'
   PRINT, 'Car_senescence      = ', Car_senescence, ' days since Jan 1st'
   PRINT, 'Car_rate_down       = ', Car_rate_down, ' day-1'
   PRINT, 'Cbrown_bbd          = ', Cbrown_bbd, '  unitless'
   PRINT, 'Cbrown_min          = ', Cbrown_min, '  unitless'
   PRINT, 'Cbrown_max          = ', Cbrown_max, '  unitless'
ENDIF

;--- GPP model
IF lai_min LE 0.01 THEN LUE_beta_corr = 1.5 ELSE LUE_beta_corr = 1.0
LUE_beta0 = 0.513*LUE_beta_corr
LUE_gama0 = 0.0196
LUE_tau   = 7.2                 ;--- days
LUE_Smax  = 17.3                ;--- degC
LUE_X0    = -4.                 ;--- degC
LUE_kappa = -0.389              ;--- kPa-1

;--- close parameter file
CLOSE,unit_in
FREE_LUN,unit_in

;-----------------
;--- read met data
;-----------------

;file = path_data_local + site_id + '_data.csv'
file = path_data_local + site_id + '_data_2009-2010_withBP.csv'

;--- read headers
; note: the file should at least contain these columns:
; year
; jday
; time
; Tair_instanr
; rhair_instant
; Rg_instant
; GPP_daily
; Tair_daily
; VPD_daily
; Rg_daily
; LAI
; Red_ADFC
; Green_ADFC
; Blue_ADFC
; Red_NC
; Green_NC
; Blue_NC
; MODIS_satellite
; MODIS_NDVI
n_VAR_min = 19
;---
OPENR,unit,file,/GET_LUN
s = ''
READF,unit,s
var_name = STR_SEP(STRCOMPRESS(STRTRIM(s,2)),',')
n_VAR = N_ELEMENTS(var_name)
CLOSE,unit
FREE_LUN, unit

;--- describe ASCII file's structure
data_template = {version:1.0, $
                 datastart:1L, $
                 delimiter:',', $
                 missingValue:missrec, $
                 commentSymbol:'#', $
                 fieldCount:n_VAR, $
                 fieldTypes:INTARR(n_VAR), $
                 fieldNames:STRARR(n_VAR), $
                 fieldLocations:LONARR(n_VAR), $
                 fieldGroups:INTARR(n_VAR)}
; ...(2=integer, 3=long, 4=float, 5=double, 7=string)
data_template.fieldTypes = [replicate(4,n_VAR_min-2),7,replicate(4,n_var-n_var_min+1)]
data_template.fieldNames = var_name
data_template.fieldLocations = indgen(n_VAR)
data_template.fieldGroups = indgen(n_VAR)

;--- read file and compute total nb of records
nlines2skip = 2
dummy = READ_ASCII( file, $
                    COUNT = n, $
                    DATA_START=nlines2skip, $
                    TEMPLATE=data_template)
ii = WHERE(FIX(dummy.year) EQ year_simul)
FOR i = 0, n_VAR-1 DO BEGIN
   cmd = var_name[i] + '_OBS = dummy.' + var_name[i] + '[ii]'
   res = execute(cmd)
ENDFOR
;---
n_date = N_ELEMENTS(year_OBS)
RGB_signal_ADFC_OBS = REPLICATE(missval,3, n_date)
GEI_signal_ADFC_OBS = REPLICATE(missval, n_date)
ii = WHERE(Red_ADFC_OBS NE missval AND Green_ADFC_OBS NE missval AND Blue_ADFC_OBS ne missval, nii)
IF nii GT 0 THEN BEGIN
   RGB_signal_ADFC_OBS[0,ii] =   Red_ADFC_OBS[ii] / (Red_ADFC_OBS[ii] + Green_ADFC_OBS[ii] + Blue_ADFC_OBS[ii])
   RGB_signal_ADFC_OBS[1,ii] = Green_ADFC_OBS[ii] / (Red_ADFC_OBS[ii] + Green_ADFC_OBS[ii] + Blue_ADFC_OBS[ii])
   RGB_signal_ADFC_OBS[2,ii] =  Blue_ADFC_OBS[ii] / (Red_ADFC_OBS[ii] + Green_ADFC_OBS[ii] + Blue_ADFC_OBS[ii])
   GEI_signal_ADFC_OBS[ii] = 2.*RGB_signal_ADFC_OBS[1,ii] - (RGB_signal_ADFC_OBS[0,ii]+RGB_signal_ADFC_OBS[2,ii])
ENDIF
;---
RGB_signal_NC_OBS = REPLICATE(missval,3, n_date)
GEI_signal_NC_OBS = REPLICATE(missval, n_date)
ii = WHERE(Red_NC_OBS NE missval AND Green_NC_OBS NE missval AND Blue_NC_OBS ne missval, nii)
IF nii GT 0 THEN BEGIN
   RGB_signal_NC_OBS[0,ii] =   Red_NC_OBS[ii] / (Red_NC_OBS[ii] + Green_NC_OBS[ii] + Blue_NC_OBS[ii])
   RGB_signal_NC_OBS[1,ii] = Green_NC_OBS[ii] / (Red_NC_OBS[ii] + Green_NC_OBS[ii] + Blue_NC_OBS[ii])
   RGB_signal_NC_OBS[2,ii] =  Blue_NC_OBS[ii] / (Red_NC_OBS[ii] + Green_NC_OBS[ii] + Blue_NC_OBS[ii])
   GEI_signal_NC_OBS[ii] = 2.*RGB_signal_NC_OBS[1,ii] - (RGB_signal_NC_OBS[0,ii]+RGB_signal_NC_OBS[2,ii])
ENDIF
;--- filter data out according to Toshie's recommendations
IF site_id EQ 'UKHam' THEN BEGIN
   ii = WHERE(year_OBS EQ 2009 OR jday_OBS LE 71)
   RGB_signal_NC_OBS[*,ii] = missval
   GEI_signal_NC_OBS[ii] = missval
ENDIF

;-----------------
;--- define dates and dimensions
;-----------------
timestep = CEIL(365./n_date)    ;--- days
jday365 = jday_OBS
jday = jday_OBS + JULDAY(1,1,year_OBS,12,0,0)-1
CALDAT,jday,month,day,year
;---
n_sim = 3
IF Car_lag_max EQ 0 THEN n_sim = 1
RGB_signal = FLTARR(3, n_date, n_sim)
RGB_signal300 = FLTARR(3, n_date, n_sim)
Car_tab = FLTARR(n_date, n_sim)
GPP_tab = FLTARR(n_date, n_sim)
NDVI_signal = FLTARR(n_date, n_sim)
title_tab = STRARR(n_sim)

;############################
; MAIN LOOP ON SIMULATIONS
;############################

FOR i_sim=0,n_sim-1 DO BEGIN

   ;--- specs of the canopy
   ;--- 1. lai (m2/m2)
   a = [lai_min,$               ;--- baseline (um/cm2)
        lai_max-lai_min,$       ;--- amplitude
        bbd*lai_rate_up,$
        lai_rate_up,$
        senescence*lai_rate_down,$
        lai_rate_down]
   Richardson_function,jday365, a, lai_tab
   lai_tab = lai_tab > 0.01

   ;--- 2. leaf mass (g/cm2)
   ;--- note: increasing Cm_max increases the amplitude of the blue and
   ;--- green bumps, but only very, very, slightly
   Cm_tab = replicate(Cm_max, n_date) ;0.5 * (1.0 + lai_tab / MAX(lai_tab)) * Cm_max

   ;--- 3. chlorophyll (Cab) contents (um/cm2)
   a = [Cab_min,$               ;--- baseline (um/cm2)
        Cab_max-Cab_min,$       ;--- amplitude
        Cab_bbd*Cab_rate_up,$
        Cab_rate_up,$
        Cab_senescence*Cab_rate_down,$
        Cab_rate_down]
   Richardson_function2,jday365, a, Cab_tab
   ii = WHERE(lai_tab EQ MIN(lai_tab))
   Cab_tab[ii] = 0.

   ;--- 4. carotenoid  (Car) contents (um/cm2)
   a = [Car_min,$               ;--- baseline (um/cm2)
        Car_max-Car_min,$       ;--- amplitude
        Car_bbd*Car_rate_up,$
        Car_rate_up,$
        Car_senescence*Car_rate_down,$
        Car_rate_down]
   Richardson_function2,jday365, a, Car_tab0
   ;shift = - (i_sim - FIX(n_sim/2)) * FIX(Car_lag_max/timestep)
   ;Car_tab[*,i_sim] = (SHIFT(Car_tab0, +shift)) < Cab_tab
   ;Car_tab[0:((shift-1)>0),i_sim] = Car_min
   Car_tab[*,i_sim] = Car_tab0
   ii = WHERE(lai_tab EQ MIN(lai_tab))
   Car_tab[ii,i_sim] = 0.

   ;--- 5. equivalent water thickness (cm)
   ;--- note: does not change anything in the visible, but even on the NDVI!
   Cw_tab = REPLICATE(Cw_0, n_date)

   ;--- 6. structure coefficient
   Ns_tab = REPLICATE(Ns_max, n_date)
   a = [Ns_min,$        ;--- baseline
        Ns_max-Ns_min,$ ;--- amplitude
        (senescence)*ABS(lai_rate_down),$
        ABS(lai_rate_down),$
        1000.*(-0.01),$
        -0.01]
   Richardson_function2,jday365, a, Ns_tab
   ;---
   ;ii = WHERE(lai_tab lt lai_max and Ns_tab le Ns_min*1.01)
   ;Ns_tab[ii] = (Ns_tab[ii]-Ns_bbd) * lai_tab[ii]/max(lai_tab[ii]) + Ns_bbd
   ;Ns_tab = SHIFT(Ns_tab, +15)

   ;--- 7. brown pigments
   ;--- note: brown pigment content is in arbitrary units (0.0001-8)
   Cbrown_tab = Cbrown_min + $
                (Cbrown_max-Cbrown_min)*(Ns_tab-MIN(Ns_tab))/(MAX(Ns_tab)-MIN(Ns_tab))
   ;a = [Cbrown_min,$            ;--- baseline
   ;     Cbrown_max-Cbrown_min,$ ;--- amplitude
   ;     Car_senescence*ABS(Car_rate_down),$
   ;     ABS(Car_rate_down),$
   ;     1000.*(-0.01),$
   ;     -0.01]
   ;Richardson_function2,jday365, a, Cbrown_tab
   ;---
   ;ii = WHERE(Car_tab lt Car_max and Cbrown_tab le Cbrown_min*1.01)
   ;Cbrown_tab[ii] = (Cbrown_tab[ii]-Cbrown_bbd) * Car_tab[ii]/max(Car_tab[ii]) + Cbrown_bbd

   ;--- 8. leanf angle
   jday0 = bbd-(lai_max-lai_min)/2./lai_rate_up
   jday1 = bbd+(lai_max-lai_min)/2./lai_rate_up
   leaf_angle_tab = leaf_angle_at_bbd +  (leaf_angle-leaf_angle_at_bbd) * ((((jday365-jday0)/(jday1-jday0))>0.)<1.)
   ;leaf_angle_tab = REPLICATE(leaf_angle, n_date)

   ;########################
   ; MAIN LOOP ON DATES
   ;########################
   Tair_tab = FLTARR(n_date)
   Rg_tab = FLTARR(n_date)
   Rgfrac_tab = FLTARR(n_date)
   Rdfrac_tab = FLTARR(n_date)
   APAR_tab = FLTARR(n_date, n_sim)
   FOR i=0,n_date-1 DO BEGIN

   ;--- phase obs data
   i_OBS = (WHERE(jday_OBS EQ jday365[i]))[0]

   ;--- input for PROSPECT5
   Cab = Cab_tab[i]         ;-- chlorophyll content (µg.cm-2) (0-100)
   Car = Car_tab[i,i_sim]   ;-- carotenoid content (µg.cm-2)  (0-40)
   Ns = Ns_tab[i]           ;-- structure coefficient (should change with turgescence) (1-4.5)
   Cbrown = Cbrown_tab[i]   ;-- brown pigment content (arbitrary units) (0.0001-8)
   Cw = Cw_tab[i]           ;-- EWT (cm) - Equivalent water thickness (0.001-0.15)
   Cm = Cm_tab[i]           ;-- LMA (g.cm-2) (0.001-0.04)

   ;--- solar parameters
   sun_azim = FLTARR(1)
   dmean_to_d_ratio=FLTARR(1)
   ogee_solar_parameters,$
      ;--- input
      latitude, $
      longitude180, $
      year[i], $
      jday[i]-JULDAY(1,1,year[i],12,0,0)+1, $
      time_OBS[i_OBS], $
      ;--- output
      rg_extatm,$
      sun_height, $
      dmean_to_d_ratio=dmean_to_d_ratio,$
      sun_azim=sun_azim,$
      coszen=coszen
   ;print,'On '+autostring(fix(day[i]))+'/'+autostring(fix(month[i]))+$'/'+$
   ;      autostring(fix(year[i])) + $
   ;      ' -> Sun-Earth d/<d> ratio = '+AUTOSTRING(1./dmean_to_d_ratio,3)

   ;--- input for SAIL
   angl  = leaf_angle_tab[i]    ;-- average leaf angle (°)
   lai   = lai_tab[i]           ;-- leaf area index
   hspot = 0.05d0               ;-- hot spot (MUST BE >0, i.e. leaf dim / canopy height)
   tts   = 90d - sun_height[0]  ;-- solar zenith angle (°)
   tto   = webcam_zenith_angle  ;-- observer zenith angle (°)
   psi   = webcam_azim_angle $
         - sun_azim[0]          ;-- azimuth (°) (sign not important?)
   ;--- this parameter is unknown but do not seem very sensitive
   psoil = 0.2d0                ;-- soil coefficient (i.e. fraction of dry soil)

   ;--- set Rg
   Rg_tab[i] = Rg_instant_OBS[i_OBS]
   
   ;--- computes diffuse fraction of global radiation
   ;--- after Reindl et al. (1990) Solar Energy, 45, 1-7.
   rgfrac = Rg_instant_OBS[i_OBS]/rg_extatm[0]
   beta = sun_height[0]*!pi/180.
   mu = SIN(beta)
   Tair_OBS = Tair_instant_OBS[i_OBS] ;+ tk0
   rhair_OBS = rhair_instant_OBS[i_OBS]/100.
   IF (rgfrac LE 0.3) THEN $
      rdfrac = MIN([1., $
                    MAX([0.,1. - 2.32e-1 * rgfrac $
                         + 2.39e-2 * mu $
                         - 6.82e-4 * Tair_OBS $
                         + 1.95e-2 * rhair_OBS])]) $
   ELSE IF (rgfrac LT 0.78) THEN $
      rdfrac = MIN([0.97, $
                    MAX([0.1,1.329 - 1.716e0 * rgfrac $
                         + 2.67e-1 * mu $
                         - 3.57e-3 * Tair_OBS $
                         + 1.06e-1 * rhair_OBS])]) $
   ELSE $
      rdfrac = MIN([1., $
                    MAX([0.1, 4.26e-1 * rgfrac $
                         - 2.56e-1 * mu $
                         + 3.49e-3 * Tair_OBS $
                         + 7.34e-2 * rhair_OBS])])

   ;--- set percentage of diffuse light for PROSAIL
   IF constant_skyl THEN BEGIN
      skyl = skyl0
   ENDIF ELSE BEGIN
      skyl = rdfrac*100.
   ENDELSE
   Rdfrac_tab[i] = rdfrac ;skyl
   Rgfrac_tab[i] = rgfrac

   ;--- output from PRO4SAIL
   ;    resh : hemispherical reflectance
   ;    resv : directional reflectance
   PRO4SAIL_5b,$
      Cab,Car,Cbrown,Cw,Cm,Ns,$
      angl,lai,hspot,tts,tto,psi,psoil,skyl,$
      resh,resv, $
      ;--- input/output ADDED BY JO
      Rg=Rg_tab[i],$
      l=l, Idir=Idir, Idif=Idif, $
      Qabs_VIS=Qabs_VIS

   ;--- compute input signal to the camera
   I_tot = Idir + Idif ;--- W/m2/m
   ii = WHERE(l LE l_IR_filter AND l GE l_UV_filter, complement=jj)
   analog2digit = 1d0 ;;;1d-9 / (n_avogadro*1e-6*h_planck*c_light/(l   *1e-9))
  
   ;--- interpolate down to 300nm
   l300 = [300+findgen(MIN(l)-300), l]
   resv300 = INTERPOL(resv[0,*], l, l300)
   I_tot300 = INTERPOL(I_tot, l, l300)
   ii300 = WHERE(l300 LE l_IR_filter  AND l300 GE l_UV_filter,complement=jj300)
   analog2digit300 = 1d0 ;;;1d-9 / (n_avogadro*1e-6*h_planck*c_light/(l300*1e-9))

   IF debug THEN BEGIN
      plot,[0],[0],$
           title=stdfont+'Camera type '+camera_type+$
           ' - day '+autostring(jday365[i]),$
           charsize=1.5,$
           xminor=1, yminor=1,$
           xrange=[300,l_IR_filter*1.1], yrange=[0,100], $
           xstyle=0, ystyle=0, $
           xtitle=stdfont+'Wavelength [nm]', $
           ytitle=stdfont+'Relative intensity [%]', $
           font=!p.font, /nodata
      oplot, l, resv[0,*] * 100., thick=1
      oplot, l300, resv300[*] * 100., color=200
      oplot, l, I_tot / MAX(I_tot) * 100., thick=3
      oplot, l300, I_tot300 / MAX(I_tot300) * 100., color=200, thick=3
      oplot,[l_IR_filter,l_IR_filter], [!y.crange[0],!y.crange[1]]
      polyfill,[l_IR_filter,l_IR_filter,!x.crange[1],!x.crange[1]], $
               [!y.crange[0],!y.crange[1],!y.crange[1],!y.crange[0]], $
               orientation=45, fill=0
   ENDIF

   FOR ic=0,n_RGB-1 DO BEGIN
      qy = INTERPOL(REFORM(quantum_yield[*,ic]), $
                                 l_quantum_yield, l, /SPLINE) > 0. ;--- mol(e)/mol(photons)
      qy300 = INTERPOL(REFORM(quantum_yield[*,ic]), $
                                 l_quantum_yield, l300, /SPLINE) > 0. ;--- mol(e)/mol(photons)
      qy[jj] = 0.
      qy300[jj300] = 0.
      IF ic EQ 0 THEN qy300_tab = qy300 ELSE qy300_tab = [[qy300_tab],[qy300]]
      ;---
      RGB_signal[ic,i,i_sim] = $
         TOTAL(qy[ii] * resv[0,ii] * I_tot[ii] * analog2digit[ii]) * $
         RGB_white_balance[ic]
      RGB_signal300[ic,i,i_sim] = $
         TOTAL(qy300[ii300] * resv300[ii300] * I_tot300[ii300] * analog2digit300[ii300]) * $
         RGB_white_balance[ic]
      IF debug THEN BEGIN
         oplot, l_quantum_yield, quantum_yield[*,ic], psym=6+ic
         oplot, l, qy, linestyle=1, thick=1
         oplot, l300, qy300, linestyle=1, thick=1, color=200
      ENDIF

   ENDFOR

   ;--- plot camera specs with current interpolation curves
   IF i EQ 0 AND i_sim EQ 0 THEN $
      plot_webcam_specs, path_for_plot=path_out, $
                         camera_type=camera_type, $
                         RGB_color_tab=RGB_color_tab,$
                         l_IR_filter=l_IR_filter,$
                         l_UV_filter=l_UV_filter,$
                         l_quantum_yield=l_quantum_yield,$
                         quantum_yield=quantum_yield, $
                         l_inter = l300, $
                         qy_inter = qy300_tab

   IF debug THEN wait,0.01

   ;-- compute GPP
   beta = LUE_beta0  * ((Cab_tab[i] / Cab_max) > 0.2);* lai_tab[i]/lai_max
   gama = LUE_gama0
   kappa = LUE_kappa
   tau = LUE_tau
   Smax = LUE_Smax
   X0 = LUE_X0
   ;---
   ;--- note: to convert from noon value to entire day,
   ;    we use the ratio of Rg_daily to Rg_instant
   ;--- note: to convert from noon value to entire day,
   ;    we use the ratio of Rg_daily to Rg_instant
   APAR_tab[i,i_sim] = Qabs_VIS * Rg_daily_OBS[i_OBS]/(Rg_instant_OBS[i_OBS]) *2.02/0.48 ;--- W/m2 -> MJ/m2/d -> mol/m2/d
   Tair_tab[i] = Tair_daily_OBS[i_OBS]                                                   ;--- degC
   VPD = VPD_daily_OBS[i_OBS]*0.1                                                        ;--- kPa
   IF i EQ 0 THEN X = Tair_tab[i] ELSE X = X + (Tair_tab[i]-X) / tau
   S = (X-X0) > 0.
   GPP_tab[i,i_sim] = beta*APAR_tab[i,i_sim] / (gama*APAR_tab[i,i_sim] + 1.) $
                      * ((S/Smax)<1.) $
                      * EXP(kappa*VPD) ;--- gC/m2/d
   ;print,'GPP = '+AUTOSTRING(GPP_tab[i,i_sim],1)+' gC/m2, APAR='+AUTOSTRING(APAR_tab[i,i_sim],1)+'mol/m2 ,'+$
   ;      'APAR/Rg = '+AUTOSTRING(Qabs_VIS/Rg_instant_OBS[i_OBS],2)

   ;--- compute NDVI
   ;ii_VIS = WHERE(l GE 580 AND l LE  680) ;--- AVHRR RED
   ;ii_NIR = WHERE(l GE 725 AND l LE 1100) ;--- AVHRR NIR
   ii_VIS = WHERE(l GE 545 AND l LE  565) ;--- MODIS RED
   ii_NIR = WHERE(l GE 841 AND l LE  871) ;--- MODIS NIR
   ;---
   VIS = TOTAL(resh[0,ii_VIS] * I_tot[ii_VIS]) 
   NIR = TOTAL(resh[0,ii_NIR] * I_tot[ii_NIR])
   NDVI_signal[i,i_sim] = (NIR - VIS) / (VIS + NIR)

ENDFOR

;--- normalise
fRGB = RGB_signal
FOR ic=0,n_RGB-1 DO BEGIN
   fRGB[ic,*,i_sim] = RGB_signal[ic,*,i_sim] / TOTAL(RGB_signal[*,*,i_sim],1)
ENDFOR
RGB_signal = fRGB
;---
fRGB = RGB_signal300
FOR ic=0,n_RGB-1 DO BEGIN
   fRGB[ic,*,i_sim] = RGB_signal300[ic,*,i_sim] / TOTAL(RGB_signal300[*,*,i_sim],1)
ENDFOR
RGB_signal300 = fRGB

ENDFOR

;--- compute modelled Hue
RGB_index = FIX(RGB_signal*255/MAX(RGB_signal))
M_tab = FLOAT(REFORM(MAX(RGB_index, DIMENSION=1, MIN=mm_tab)))
C_tab = FLOAT(M_tab - REFORM(mm_tab))
HSL_tab = REPLICATE(0., n_date, n_sim)
FOR i=0,n_date-1 DO BEGIN
   FOR i_sim=0,n_sim-1 DO BEGIN
      IF C_tab[i,i_sim] GT 0. AND M_tab[i,i_sim] EQ RGB_index[0,i,i_sim] THEN $
         HSL_tab[i,i_sim] = (RGB_index[1,i,i_sim] - RGB_index[2,i,i_sim])/C_tab[i,i_sim] MOD 6
      IF C_tab[i,i_sim] GT 0. AND M_tab[i,i_sim] EQ RGB_index[1,i,i_sim] THEN $
         HSL_tab[i,i_sim] = (RGB_index[2,i,i_sim] - RGB_index[0,i,i_sim])/C_tab[i,i_sim] + 2
      IF C_tab[i,i_sim] GT 0. AND M_tab[i,i_sim] EQ RGB_index[2,i,i_sim] THEN $
         HSL_tab[i,i_sim] = (RGB_index[0,i,i_sim] - RGB_index[1,i,i_sim])/C_tab[i,i_sim] + 4
   ENDFOR
ENDFOR
HSL_tab = HSL_tab * 60

;----------------------
;   PLOT TIME SERIES
;----------------------

;--- open ps file
n_plot_timeseries = 5 ;6
xboxsize = 6.0 ;5.5
xboxleft = 1.3
xboxright = 0.2
yboxsize = 2.8 ;2.6
yboxbot = 1.0
yboxtop = 0.5 ;2.6
xsize = xboxsize                   + xboxleft + xboxright
ysize = yboxsize*n_plot_timeseries + yboxbot  + yboxtop
;---
charsize = 1.6
symsize = 0.5 ;0.3
charsize_leg = 0.6
line_thickness  = 1
IF n_sim EQ 1 AND constant_skyl EQ 1 THEN line_thickness = 2
;---
set_plot, 'ps'
!p.font=0
IF constant_skyl THEN skyl_tit='constantSKYL' ELSE skyl_tit='variableSKYL'
file = path_out + simul_id + '_' + skyl_tit + '_timeseries.ps'
device, filename=file, encapsulated=1, /helvetica, /color
device, xsize=xsize, ysize=ysize

;--- define title
;title0 = stdfont + $
;         autostring(FIX(longitude180))+s_degree+stdfont+'W - ' +$
;         autostring(FIX(latitude))+s_degree+stdfont+'N!c!c' +$
;         ;---
;         s_theta+stdfont+'!dv!n = '+autostring(FIX(tto))+s_degree+stdfont+' - '+$
;         s_phi+stdfont+'!dv!n = '+autostring(FIX(psi+sun_azim[0]))+s_degree+stdfont+'!c!c' +$
;         ;---
;         'C!dw!n = '+autostring(Cw_0,3)+'cm - ' +$
;         'LMA = '+autostring(FIX(Cm_max*1e4))+'g/cm!u2!n - ' +$
;         s_psi+stdfont+'!dleaf!n = '+autostring(FIX(angl))+s_degree+stdfont+'!c!c'+$
;         's!dhotspot!n = '+autostring(hspot,2)+' - ' +$
;         s_phi+stdfont+'!ddry,soil!n = '+autostring(psoil,1)+' - ' +$
;         'SKYL = '
;IF constant_skyl THEN title0 = title0 + autostring(FIX(skyl0))+'%' $
;ELSE title0 = title0 + 'variable'
IF constant_skyl THEN skyl_tit0 = 'constant' ELSE skyl_tit0 = 'variable'
title0 = camera_type + ' camera - '+ skyl_tit0 + ' SKYL'

;--- define arrays ot be plotted
xarr = jday365
xarr_OBS = jday_OBS+JULDAY(1,1,year_OBS)-JULDAY(1,1,year_OBS[0])
xtitle0 = stdfont+'Days since Jan 1!ust!n'
IF site_id NE '' THEN xtitle0 = xtitle0+', '+AUTOSTRING(FIX(year_OBS[0]))
yticks = 6
xstyle_default = 1
ystyle_default = 1
dummy = REPLICATE(missval,n_date)
ii=where(lai_tab LE min(lai_tab))
Cab_tab[ii] = missval
Car_tab[ii] = missval
Ns_tab[ii] = missval
Cbrown_tab[ii] = missval
yarr = [ $
       [Cab_tab], $
       [Ns_tab], $
       [lai_tab], $
       ;[dummy], $
       [dummy], $
       [dummy] $
       ]
ytitle0 = stdfont + [ $
          'Chl and Car content !c['+s_micro+stdfont+'g cm!u-2!n]', $
          'Structural (N) and!cbrown pigment coeff. [-]', $
          'LAI proxy!c[m!u2!n m!u-2!n]', $
          'RGB fraction!c',$
          ;'Green Fraction !cbp frequency [%]', $
          'Gross Primary Productivity!c[gC m!u-2!n d!u-1!n]' $
                    ] + stdfont
ymin0          = [-5, -0.15, -0.2, 0.21, 0] ;0, 0]
ytickinterval0 = [20,  1.0, FIX(lai_max/3), 0.04, 4] ;10, 4]
IF site_id EQ 'UKHam' AND camera_type EQ 'ADFC' THEN BEGIN
   ymin0[3] = 0.16
   ytickinterval0[3] = 0.07
ENDIF

;--- define plot coordinates
nxplot = 1
nyplot = N_ELEMENTS(ytitle0)
nplot = nxplot*nyplot
xbox_size = replicate(xboxsize/xsize,nxplot)
ybox_size = replicate(yboxsize/ysize,nyplot)
xbox_min = fltarr(nxplot)
ybox_max = fltarr(nyplot)
xbox_min[0] = xboxleft/xsize
ybox_max[0] = 1.-yboxtop/ysize
for iplot=1,nxplot-1 do xbox_min[iplot] = xbox_min[iplot-1] + xbox_size[iplot-1]
for iplot=1,nyplot-1 do ybox_max[iplot] = ybox_max[iplot-1] - ybox_size[iplot-1]
!p.multi = [0,nxplot,nyplot]
color_tab = REPLICATE(0, nyplot)
color_tab[0] = 4

;--- draw a frame
xrange = [1,365]
IF constant_skyl THEN BEGIN
   ;jj_nocloud = LINDGEN(n_date)
   ;jj_cloud = jj_nocloud
   jj_nocloud = WHERE(Rdfrac_tab LE 0.5, complement=jj_cloud)
ENDIF ELSE BEGIN
   jj_nocloud = WHERE(Rdfrac_tab LE 0.5, complement=jj_cloud)
ENDELSE

FOR iplot = 0,nplot-1 DO BEGIN
   iyplot = fix(iplot/nxplot)
   ixplot = iplot mod nxplot

   ;--- define position
   position = [xbox_min[ixplot]                  , $
               ybox_max[iyplot]-ybox_size[iyplot], $
               xbox_min[ixplot]+xbox_size[ixplot], $
               ybox_max[iyplot]]

   ;--- draw a frame
   yrange = [ymin0[iyplot],ymin0[iyplot]+(yticks-0.5)*ytickinterval0[iyplot]]
   IF iyplot EQ nyplot-1 THEN BEGIN
      xtickname = ''
      xtitle = xtitle0
      xstyle = xstyle_default
   ENDIF ELSE BEGIN
      xtickname = replicate(' ',10)
      xtitle = stdfont+''
      xstyle = xstyle_default
   ENDELSE
   if ixplot eq 0 then begin
      ytitle = stdfont+''
      ystyle = ystyle_default
      ytickname =  [replicate('',yticks),' ']
   endif else begin
      ytitle = stdfont+''
      ystyle = ystyle_default
      ytickname = replicate(' ',10)
   endelse
   ;---
   plot,[0],[0],$
        charsize=charsize,charthick=charthick,position=position,$
        xthick=axis_thickness,ythick=axis_thickness,xminor=1,yminor=1,$
        xrange=xrange, xtitle=xtitle, xstyle=xstyle, xtickname=xtickname,$
        yrange=yrange , ytitle=ytitle, ystyle=ystyle, ytickname=ytickname,$
        yticks=yticks, ytickinterval=ytickinterval0[iyplot], $
        yticklen=0.01,xtickinterval=0,$
        color=0, font=!p.font
   if iyplot eq 0 then begin
      ;axis,xaxis=1,charsize=charsize*0.8,charthick=charthick,$
      ;     xtitle = title0,$
      ;     xthick=axis_thickness,xminor=1,$
      ;     xrange=xrange, xstyle=xstyle, xtickname=replicate(' ',10),$
      ;     color=0, font=!p.font
;      xyouts,0.58,0.95, charsize=charsize*0.4,charthick=charthick,$
;           title0,color=0,/normal, alignment=0.5
    endif

   ;--- write y-axis title
   x_csize_def = !D.X_CH_SIZE
   y_csize_def = !D.Y_CH_SIZE
   DEVICE, SET_CHARACTER_SIZE=[70,500] ;--- play on the second number to change the line spacing
   IF ixplot EQ 0 THEN $
      xyouts,xbox_min[0]/4.,$
             ybox_max[iyplot] - ybox_size[iyplot]*0.5,$
             ytitle0[iplot], ALIGNMENT=0.5, ORIENTATION=90., $
             CHARSIZE=charsize_leg,/NORMAL, color=0
   DEVICE, SET_CHARACTER_SIZE=[x_csize_def,y_csize_def] ;--- reset to default


   ;--- plot default curve
   oplot,xarr, yarr[*,iyplot], $
         color=color_tab[iyplot], thick=line_thickness*(1+n_sim), $
         linestyle=0, min_value=missval+1

   ;--- eventually plot more data...

   ;--- ...LAI
   IF iyplot EQ 2 THEN BEGIN
      ccg_symbol,sym=2,fill=0
      oplot,xarr_OBS[jj_cloud], $
            LAI_OBS[jj_cloud], $
            color=0, psym=8, symsize=symsize, min_value=missval+1
      ccg_symbol,sym=2,fill=1
      oplot,xarr_OBS[jj_nocloud], $
            LAI_OBS[jj_nocloud], $
            color=0, psym=8, symsize=symsize, min_value=missval+1
   ENDIF

   ;--- ... RGB data
   IF iyplot EQ 3 THEN BEGIN
      IF camera_type EQ 'ADFC' THEN RGB_signal_OBS = RGB_signal_ADFC_OBS ELSE RGB_signal_OBS = RGB_signal_NC_OBS
      ccg_symbol,sym=2,fill=1
      FOR ic=0,n_RGB-1 DO $
         oplot,xarr_OBS[jj_nocloud], $
               RGB_signal_OBS[ic,jj_nocloud], $
               color=RGB_color_TAB[ic], psym=8, symsize=symsize, min_value=missval+1
      ccg_symbol,sym=2,fill=0
      FOR ic=0,n_RGB-1 DO $
         oplot,xarr_OBS[jj_cloud], $
               RGB_signal_OBS[ic,jj_cloud], $
               color=RGB_color_TAB[ic], psym=8, symsize=symsize, min_value=missval+1
   ENDIF

   ;--- ... break point freq.
   ;IF iyplot EQ 4 AND camera_type EQ 'ADFC' THEN BEGIN
   ;   ;oplot,xarr_OBS, $
   ;   ;      break_point_frequency_OBS, $
   ;   ;      color=0, psym=10, min_value=missval+1
   ;   bar_plot,break_point_frequency_OBS,$
   ;            baselines=replicate(0,n_date),$
   ;            colors=replicate(150,n_date), $
   ;            /overplot;, /outline
   ;ENDIF

   ;--- ... GPP (model and data)
   IF iyplot EQ 4 THEN BEGIN
      mc_symbol,sym=2,fill=1
      oplot,xarr_OBS, $
            GPP_daily_OBS, $
            color=31, psym=8, symsize=symsize, min_value=missval+1
      mc_symbol,sym=2,fill=0,thick=0.5
      oplot,xarr_OBS, $
            GPP_daily_OBS, $
            color=0, psym=8, symsize=symsize, min_value=missval+1
      ;FOR i_sim=0,n_sim-1 DO $
      ;   oplot,xarr, $
      ;         GPP_tab[*,i_sim], $
      ;         color=0, thick=line_thickness*(n_sim-i_sim+1), $
      ;         linestyle=i_sim
   ENDIF

   ;--- ... Car model
   IF iyplot EQ 0 THEN BEGIN
      FOR i_sim=0,n_sim-1 DO $
         oplot,xarr, Car_tab[*,i_sim], $
               color=11, thick=line_thickness*(n_sim-i_sim+1), $
               linestyle=i_sim, min_value=missval+1
   ENDIF

   ;--- ... brown pigment model
   IF iyplot EQ 1 THEN BEGIN
      oplot,xarr, Cbrown_tab, $
            color=69, thick=line_thickness*(1+n_sim), $
            linestyle=0, min_value=missval+1
   ENDIF
   
   ;---  ... model RGB signal
   IF iyplot EQ 3 THEN BEGIN
      FOR i_sim=0,n_sim-1 DO BEGIN
         ;ccg_symbol,sym=2,fill=1
         FOR ic=0,n_RGB-1 DO $
            oplot,xarr[jj_nocloud], $
                  RGB_signal300[ic,jj_nocloud,i_sim], $
                  color=RGB_color_TAB_mod[ic], $
                  ;psym=8, symsize=symsize, $
                  thick=line_thickness*(n_sim-i_sim+1), linestyle=i_sim
      ENDFOR

      ;--- plot period used to calibrate white balance
      ii0 = (WHERE(xarr_OBS GE 70))[0] + [0, 30]
      ;ii0 = (WHERE(TOTAL(RGB_signal_OBS[*,*],1) EQ 1))[0] + [0, 30]
      oplot, xarr_OBS[ii0], $
             [0.23,0.23], thick=18, color=8
   ENDIF

   ;IF iyplot EQ 4 THEN BEGIN
      ;   GEI_tab = REFORM(2.*RGB_signal[1,*,*] - (RGB_signal[0,*,*]+RGB_signal[2,*,*]));*255
      ;   FOR i_sim=0,n_sim-1 DO $
      ;      oplot,xarr, $
      ;            GEI_tab[*,i_sim], $
      ;            color=63, thick=line_thickness*(n_sim-i_sim+1), linestyle=i_sim
      ;   ;---
      ;   ;FOR i_sim=0,n_sim-1 DO $
      ;   ;   oplot,xarr, $
      ;   ;         HSL_tab[*,i_sim], $
      ;   ;         color=6, thick=line_thickness*(n_sim-i_sim+1), linestyle=i_sim
      ;   ;---
      ;   IF constant_skyl EQ 0 THEN BEGIN
      ;      ii = WHERE(Rdfrac_tab LE 0.30)
      ;      mc_symbol,sym=1,fill=1
      ;      psym=-8
      ;   ENDIF ELSE BEGIN
      ;      ii = INDGEN(n_date)
      ;      psym=0
      ;   ENDELSE
      ;   FOR i_sim=0,n_sim-1 DO $
      ;      oplot,xarr[ii], $
      ;            NDVI_signal[ii,i_sim], $
      ;            color=6, psym=psym,symsize=0.3, thick=line_thickness*(n_sim-i_sim+1), linestyle=i_sim
   ;ENDIF
   oplot,[bbd,bbd],!y.crange,linestyle=1
   oplot,[senescence,senescence],!y.crange,linestyle=1
   ;oplot,[Cab_bbd,Cab_bbd],!y.crange,linestyle=1

   ;--- write legends
   IF iyplot EQ 0 THEN BEGIN
      mc_llegend,$
         x=!x.crange[0]+(!x.crange[1]-!x.crange[0])*0.08,$
         y=!y.crange[0]+(!y.crange[1]-!y.crange[0])*0.85, $
         CHARSIZE=charsize_leg, SYMSIZE=symsize,$
         larr = [0,0],$
         ltarr= REPLICATE(line_thickness*(1+n_sim), 2),$
         carr = [4,11],$
         tarr=stdfont+['Chlorophyll a+b','Carotenoids'],$
         row=2,col=1,$
        llength=5,incr_text=1.2,$
         vertical=1,cspace=8.,$
         font=!p.font,/data
   ENDIF
   IF iyplot EQ 1 THEN BEGIN
      mc_llegend,$
         x=!x.crange[0]+(!x.crange[1]-!x.crange[0])*0.08,$
         y=!y.crange[0]+(!y.crange[1]-!y.crange[0])*0.85, $
         CHARSIZE=charsize_leg, SYMSIZE=symsize,$
         larr = [0,0],$
         ltarr= REPLICATE(line_thickness*(1+n_sim), 2),$
         carr = [0,69],$
         tarr=stdfont+['N','Brown pigment coeff.'],$
         row=2,col=1,$
        llength=5,incr_text=1.2,$
         vertical=1,cspace=8.,$
         font=!p.font,/data
   ENDIF
   ;IF iyplot EQ 2 or iyplot EQ 3 or iyplot EQ 5 THEN BEGIN
   ;IF iyplot EQ 2 or iyplot EQ 3 or iyplot EQ 4 THEN BEGIN
   IF iyplot EQ 2 or iyplot EQ 3 THEN BEGIN
      mc_llegend,$
         x=!x.crange[0]+(!x.crange[1]-!x.crange[0])*0.08,$
         y=!y.crange[0]+(!y.crange[1]-!y.crange[0])*0.85, $
         CHARSIZE=charsize_leg, SYMSIZE=symsize,$
         larr = [0,0],$
         sarr = [2,0],$
         ltarr= REPLICATE(line_thickness*(1+n_sim), 2),$
         carr = [0,0],$
         tarr=stdfont+['Measured','Modelled'],$
         row=2,col=1,$
        llength=5,incr_text=1.2,$
         vertical=1,cspace=8.,$
         font=!p.font,/data
   ENDIF
   ;IF iyplot EQ nyplot-1 THEN BEGIN
   ;   mc_llegend,$
   ;      x=!x.crange[0]+(!x.crange[1]-!x.crange[0])*0.10,$
   ;      y=!y.crange[0]+(!y.crange[1]-!y.crange[0])*0.85, $
   ;      CHARSIZE=charsize_leg, SYMSIZE=symsize,$
   ;      larr = [0, 0], $
   ;      ltarr= [line_thickness, line_thickness]*(1+n_sim), $
   ;      carr = [6, 63], $
   ;      tarr=stdfont+['NDVI', 'GEI'],$
   ;      row=2,col=1,$
   ;      llength=7,incr_text=1.2,$
   ;      vertical=1,cspace=8.,$
   ;      font=!p.font,/data
   ;ENDIF

ENDFOR
!p.multi=0

;--- close PS file
device, /close
set_plot, 'x'
;set_plot, 'win'
!p.font=-1

;--- converts to PDF and deletes PS file (for Mac only)
IF (os NE 'Windows') THEN BEGIN
   cmd = 'pstopdf '+file+' -o '+(STR_SEP(file,'.ps'))[0]+'.pdf'
   ;cmd = 'ps2pdf '+file+' '+(STR_SEP(file,'.ps'))[0]+'.pdf'
   spawn,cmd
   FILE_DELETE, file
ENDIF

;----------------------
;   PLOT XY PLOT
;----------------------

;--- open ps file
aspect_ratio=1.1
xsize=8
ysize=xsize/aspect_ratio
set_plot, 'ps'
!p.font=0
file = path_out + simul_id + '_' + skyl_tit + '_XY.ps'
device, filename=file, encapsulated=1, /color, /helvetica
device, xsize=xsize, ysize=ysize

;--- draw a frame
!p.multi = 0
IF constant_skyl THEN BEGIN
   ytitle='modelled RGB fraction (non-cloudy)'
   ytickname = ''
   label = '(a)'
ENDIF ELSE BEGIN
   ytitle=''
   ytickname = REPLICATE(' ',10)
   label = '(b)'
ENDELSE
plot,[0], [0], $
     position=[0.22,0.14,0.92,0.90],$ ;[0.22,0.12,0.92,0.72],$
     xrange=[0.22,0.45],yrange=[0.22,0.45], $
     xtitle='measured RGB fraction (non-cloudy)', $
     ytitle=ytitle, ytickname=ytickname,$
     charsize=0.8,$
     color=0, /nodata
oplot,!x.crange,!x.crange, linestyle=2, thick=3
;xyouts,0.22+0.35, 0.93, $
;     title0,$;+'!c!i(non-cloudy conditions only)!n',$
;       charsize=1.1,alignment=0.5,/normal
xyouts,0.22, 0.43, label, charsize=0.8

;--- plot data points
FOR ic=0,2 DO BEGIN
   ;ccg_symbol,sym=2,fill=0
   ;ii=WHERE(RGB_signal_OBS[ic,*] ne missval)
   ;oplot,RGB_signal_OBS[ic,ii], RGB_signal300[ic,ii], $
   ;     psym=8, color=RGB_color_TAB[ic], symsize=0.3
   ;---
   ccg_symbol,sym=2,fill=1
   jj=WHERE(RGB_signal_OBS[ic,jj_nocloud] ne missval)
   oplot,RGB_signal_OBS[ic,jj_nocloud[jj]], RGB_signal300[ic,jj_nocloud[jj]], $
        psym=8, color=RGB_color_TAB[ic], symsize=0.6
ENDFOR
;r = CORRELATE(RGB_signal_OBS[*,ii], RGB_signal300[*,ii])
;RMSE = SQRT(MEAN((RGB_signal_OBS[*,ii] - RGB_signal300[*,ii])^2))
;RMSEr = SQRT(MEAN((RGB_signal_OBS[0,ii] - RGB_signal300[0,ii])^2))
;RMSEg = SQRT(MEAN((RGB_signal_OBS[1,ii] - RGB_signal300[1,ii])^2))
;RMSEb = SQRT(MEAN((RGB_signal_OBS[2,ii] - RGB_signal300[2,ii])^2))
r = CORRELATE(RGB_signal_OBS[*,jj_nocloud[jj]], RGB_signal300[*,jj_nocloud[jj]])
RMSE = SQRT(MEAN((RGB_signal_OBS[*,jj_nocloud[jj]] - RGB_signal300[*,jj_nocloud[jj]])^2))
RMSEr = SQRT(MEAN((RGB_signal_OBS[0,jj_nocloud[jj]] - RGB_signal300[0,jj_nocloud[jj]])^2))
RMSEg = SQRT(MEAN((RGB_signal_OBS[1,jj_nocloud[jj]] - RGB_signal300[1,jj_nocloud[jj]])^2))
RMSEb = SQRT(MEAN((RGB_signal_OBS[2,jj_nocloud[jj]] - RGB_signal300[2,jj_nocloud[jj]])^2))
RMSE_tab = [RMSEr,RMSEg,RMSEb]
;xyouts,0.22,0.42,'r = '+autostring(r,2),$;+'!c!bRMSE = '+autostring(RMSE,2),$
;       charsize=0.8
for ic=0,2 do $
   xyouts,0.22,0.40-0.02*ic,'RMSE = '+autostring(RMSE_tab[ic],2),$
       charsize=0.8,color=RGB_color_TAB[ic]

;--- close PS file
device, /close
set_plot, 'x'
!p.font=-1

;--- converts to PDF and deletes PS file (for Mac only)
IF (os NE 'Windows') THEN BEGIN
   cmd = 'pstopdf '+file+' -o '+(STR_SEP(file,'.ps'))[0]+'.pdf'
   ;cmd = 'ps2pdf '+file+' '+(STR_SEP(file,'.ps'))[0]+'.pdf'
   spawn,cmd
   FILE_DELETE, file
ENDIF

;---
;SET_PLOT,'X'
;DEVICE, decomposed=0
;;CCG_RGBLOAD,file=!DIR+'/lib/ccg_lib/'+'data/color_comb1'
;CCG_RGBLOAD,file='./ccg_color_comb1'

;--- write output in ASCII file
IF write_ascii THEN BEGIN
   PRINT,'year, jday365, Rfrac, Gfrac, Bfrac, NDVI'
   FOR i=0,n_date-1 DO BEGIN
      PRINT, FORMAT='(i4,",",i3,4(",",F7.3))', year_OBS[i], jday365[i], RGB_signal[*,i,0], NDVI_signal[i,0]
   ENDFOR
ENDIF

stop
stop
END

