function random,seed,n,min,max,normal_dist=normal_dist
IF normal_dist EQ 0 THEN $
   res = min + (max-min) * RANDOMU(SEED, n) $
ELSE $
   res = ((0.5*(max+min) + (max-min)/6. * RANDOMN(SEED, n)) > min) < max
return,res
end


;+
; DESCRIPTION:
; This program performs a sensitivity analysis of webcam RGB signals
; to main PROSAIL parameters:
; - 4SAIL parameters: LAI, SKYL, leaf_angle, f_dry_soil, s_hotspot
; - PROSPECT parameters: Cab, Car, Cm, Cw, Cbrown, N
;
; Canopy directional reflectance modelling is based on PROSAIL:
; - modeling leaf optical properties with PROSPECT-5 (Feret et al. 2008)
; - modeling leaf inclination distribution function using Campbell's
;   ellipsoidal distribution function caracterised by the average leaf 
;   inclination angle in degree
; - modeling canopy reflectance with 4SAIL (Verhoef et al., 2007)
; - modeling incoming solar spectra for direct and diffuse light based
;   on 6S simulations from Francois et al. (2002)
; 
; REFERENCES:
; Verhoef et al. (2007) Unified Optical-Thermal Four-Stream Radiative
;   Transfer Theory for Homogeneous Vegetation Canopies, IEEE TRANSACTIONS 
;  ON GEOSCIENCE AND REMOTE SENSING, 45(6).
; Féret et al. (2008), PROSPECT-4 and 5: Advances in the Leaf Optical
;   Properties Model Separating Photosynthetic Pigments, REMOTE SENSING OF 
;  ENVIRONMENT.
; Francois et al. (2002) Conversion of 400–1100 nm vegetation albedo 
;    measurements into total shortwave broadband albedo using a canopy 
;    radiative transfer model. AGRONOMY, 22.
;
; NAME: main
;
; AUTHOR/HISTORY:
; Original Matlab code written by Jean-Baptiste Féret (feret@ipgp.fr)
;    Institut de Physique du Globe de Paris, Space and Planetary Geophysics
;    during October 2009 based on a version of PROSAIL provided by
;    Wout Verhoef, NLR on April/May 2003.
; Translated from Matlab to IDL by Eben N. Broadbent (ebennb@gmail.com)
;    Dept. of Biology, Stanford University.
; Adapted for Webcam studies by Jerome Ogee (jogee@bordeaux.inra.fr)
;    INRA, UR 1263, 33140 Villenave d'Ornon, France
;
; CONTACT INFO: jogee@bordeaux.inra.fr
;
; CALLING SEQUENCE:
;       camera_type = camera_type, $
;       altogether_flag=altogether_flag,$
;       add_NDVI_flag = add_NDVI_flag
;
; INPUTS: none, parameters are set within the procedure
;
; OUTPUTS: none (only plots)
;
; OPTIONAL INPUT(S): camera_type (type of webcam used)
;
; OPTIONAL OUTPUT(S): none
;
; OPTIONAL INPUT KEYWORD(S):
; - altogether_flag to perform sensitivity with all parametersvarying together
; - correlation_flag to introduce correlation between parameters
; - add_NDVI_flag to add NDVI sensitivity to the analysis
;
; NOTES:
; The specific absorption coefficient corresponding to brown pigment is
;   provided by Frederic Baret (EMMAH, INRA Avignon, baret@avignon.inra.fr)
;   and used with his autorization.
; The model PRO4SAIL is based on a version provided by Wout Verhoef (NLR)
;   in April/May 2003
;
; METHOD:
;
; EXAMPLE:
;
; MODIFICATION HISTORY:
; 09/10/2010 Downloaded from "http://teledetection.ipgp.jussieu.fr/prosail/"
; 09/27/2010 Translated from matlab to IDL by Eben N. Broadbent
; March 2012 Adapated for Webcam studies by Jerome Ogee
;
; CODING NOTES:
; Same as downloaded Matlab version
; Some more comments added on IDL version by J. Ogee
;-

;@@@@@@@@@@@@@@@@@@@@@@@@
pro main, camera_type = camera_type, $
          altogether_flag = altogether_flag,$
          correlation_flag = correlation_flag,$
          add_NDVI_flag = add_NDVI_flag,$
          phenology_period = phenology_period, $
          n_sim = n_sim
;@@@@@@@@@@@@@@@@@@@@@@@@

@ogee_general_header.inc

;--- physical constants
h_planck = 6.62606957d-34       ;--- J.s
c_light = 299792458d0           ;--- m/s
n_avogadro = 6.023e23           ;--- mol-1

;--- note: KEYWORD_SET returns zero if argument is set to zero... so
;    the default values should be zero and only when we want something
;    different from zero we need to specify it...
IF NOT KEYWORD_SET(camera_type) THEN camera_type = 'NC'
IF NOT KEYWORD_SET(altogether_flag) THEN altogether_flag = 0
IF NOT KEYWORD_SET(correlation_flag) THEN correlation_flag = 0
IF NOT KEYWORD_SET(add_NDVI_flag) THEN add_NDVI_flag = 0
IF NOT KEYWORD_SET(phenology_period) THEN phenology_period = 'allseason';'budburst' ;'senescence'

normal_dist = 0;1;0;1
;---
IF altogether_flag EQ 0 THEN correlation_flag = 0

;--- re-adjust folders
SPAWN,wdir,path0
path_out        = path0 + dl + 'io_files' + dl

;--- main flags
restore_flag = 0 ;1 ;0
IF altogether_flag EQ 0 THEN restore_flag = 0

;--- store default color tables to reset whenever we want to
;CCG_RGBLOAD,file=!DIR+'/lib/My_IDL/ccg_lib/'+'data/color_comb1'
CCG_RGBLOAD,file='./ccg_color_comb1'
TVLCT,red,green,blue,/GET

;--- filename
file = path_out+'PROSAIL_sensitivity_'+camera_type+$
       '_altogether='+AUTOSTRING(altogether_flag)+$
       '_correlation='+AUTOSTRING(correlation_flag)+'_'+phenology_period+'.ps'
;---
restore_file = (STR_SEP(file,'.ps'))[0]+'.sav'
joinpdf = '/System/Library/Automator/Combine\ PDF\ Pages.action/Contents/Resources/join.py -o '

;--- initialisation of MC simulation
SEED = 0L
IF NOT KEYWORD_SET(n_sim) THEN n_sim = 2000L
IF altogether_flag EQ 0 THEN n_sim = 100L

;-----------------
;--- specs of the camera
;-----------------
;    note: we add an extra wavelength to insure the
;    interpolation does not go crazy for > 750nm...
CASE camera_type OF
   'NC': BEGIN
      webcam_zenith_angle = 80d0
      webcam_azim_angle   = 0d0
      ;--- to match observed RGB signal (see UKHam_NC_2010.in)
      RGB_white_balance   = [255, 168, 185] ;[255, 230, 255]
      ;---
      l_quantum_yield     = [380, 390,400,425,450,475,500,525,530,550,575,600,625,650,675,700,725,750,760]
      quantum_yield_red   = [  0, 6,  5,  2,1.5,  2,3.5,7.5,7.5,  7, 10, 37, 30, 27, 20, 19, 19, 20, 20]
      quantum_yield_green = [  0, 5,  5,5.5,  9, 20, 30, 45, 46, 40, 25, 13,  7,  6,7.5, 10, 10, 12, 12]
      quantum_yield_blue  = [ 0, 17, 26, 37, 43, 36, 20, 10,  9,  7,  5,4.5,  4,  4,  4,  5,  5,  6,  6]
      ;--- from last email from Daniel Lawton and Anthony Watts
      ;    IR cutoff=650nm and UV cutoff is below 400nm
      l_IR_filter = 650.
      l_UV_filter = 380.
   END
   'ADFC': BEGIN
      webcam_zenith_angle = 10d0
      webcam_azim_angle   = 0d0
      ;--- to match observed RGB signal (see UKHam_ADFC_2010.in)
      RGB_white_balance   = [235, 190, 255] ;[210, 185, 255] 
      ;---
      l_quantum_yield     = [400,425,440,450,465,475,500,512,525,540,575,590,610,625,650,670,685,700]
      quantum_yield_red   = [  0, 14, 18, 17, 13,  6,  0,  0,  0,  0, 42, 77, 86, 70, 32,  9,  0,  0]
      quantum_yield_green = [  0, 11, 10, 12, 23, 42, 77, 91,100, 89, 42,  9,  0,  0,  0,  0,  0,  0]
      quantum_yield_blue  = [  0, 40, 70, 80, 83, 80, 40, 20,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0]
      l_IR_filter = 685.
      l_UV_filter = 380. ;???
   END
   ELSE: BEGIN
      PRINT,'Type of camera not known. Only NC- and ADFC-type are possible'
      STOP
   END
ENDCASE
;---
quantum_yield = [ [quantum_yield_red   ] ,$
                  [quantum_yield_green ] ,$
                  [quantum_yield_blue  ] ]

;--- convert into normalised values
FOR ic=0,2 DO BEGIN
   ii = where(l_quantum_yield le l_UV_filter OR l_quantum_yield GE l_IR_filter,nii)
   IF nii GT 0 THEN quantum_yield[ii,ic] = 0.
   IF camera_type EQ 'NC' THEN quantum_yield[*,ic] = quantum_yield[*,ic] * (l_quantum_yield/1e3)
ENDFOR
factor =  100. / MAX(quantum_yield)
quantum_yield = quantum_yield * factor

;--- color scheme
n_RGB = 3
RGB_color_TAB =  [2, 63, 23]
RGB_title_TAB = ['Red', 'Green', 'Blue']
IF add_NDVI_flag THEN BEGIN
   n_RGB = 4
   RGB_color_TAB =  [2, 63, 23, 7]
   RGB_title_TAB = ['Red', 'Green', 'Blue', 'NDVI']
ENDIF

;-----------------
;--- spec of the site
;-----------------
latitude     = 51.12
longitude180 = -0.85

;-----------------
;--- list of parameter to be tested
;-----------------
param_list = ['sun_height',$
              'webcam_zenith_angle',$
              'skyl',$
              ;---
              'leaf_angle',$
              'lai',$
              'hspot',$
              'psoil', $
              ;---
              'Cab',$
              'Car',$
              'Cbrown',$
              'Cw',$
              'Cm',$
              'Ns']
n_param = N_ELEMENTS(param_list)
;---
param_title = ['Sun height ['+s_degree+stdfont+']',$ ;0
               'View angle ['+s_degree+stdfont+']',$ ;1
               '% diffuse light',$ ;2
               ;---
               'Leaf angle ['+s_degree+stdfont+']',$ ;3
               'LAI [m!u2!n m!u-2!n]',$ ;4 (NEVER CONSTANT)
               'Hotspot param. [-]',$ ;5
               'Dry soil fract.',$ ;6
               ;---
               'Chl ['+s_micro+stdfont+'g cm!u-2!n]',$ ;7 (NEVER CONSTANT)
               'Car ['+s_micro+stdfont+'g cm!u-2!n]',$ ;8 (NEVER CONSTANT)
               'C!dbrown',$ ;9
               'EWT [mm]',$ ;10
               'LMA [g m!u-2!n]',$ ;11
               'N'] ;12

;--- param_flag = 1 then included in sensitivity analysis
;    param_flag = 0 means parameter not included in the sensitivity analysis
param_flag = REPLICATE(1, n_param)
CASE phenology_period OF
   'budburst':   param_flag[[0,1,2,3,5,6,9,10,11,12]] = 0 ;--- budburst
   'senescence': param_flag[[0,1,2,3,5,6,  10,11   ]] = 0 ;--- senescence
   ELSE:         param_flag[[0,1]] = 0                    ;--- all
ENDCASE

;-----------------
;--- view angle
;-----------------
webcam_zenith_angle_tab = RANDOM(SEED, n_sim, 0d0, 90d0, normal_dist=normal_dist)
IF param_flag[WHERE(param_list EQ 'webcam_zenith_angle')] EQ 0 AND $
   altogether_flag EQ 1 THEN $
      webcam_zenith_angle_tab = REPLICATE(webcam_zenith_angle, n_sim)

;-----------------
;--- time variables (and solar angle)
;-----------------
year        = 2010
time        = 12.
jday365_tab = RANDOM(SEED, n_sim, 0d0, 365d0, normal_dist=normal_dist)
sun_azim_tab = FLTARR(n_sim)
ogee_solar_parameters,$
   ;--- input
   latitude, $
   longitude180, $
   year, $
   jday365_tab, $
   time, $
   ;--- output
   rg_extatm_tab,$
   sun_height_tab,$
   sun_azim=sun_azim_tab
sun_height_0 = 60.
IF param_flag[WHERE(param_list EQ 'sun_height')] EQ 0 AND $
   altogether_flag EQ 1 THEN $
      sun_height_tab = REPLICATE(sun_height_0, n_sim)

;-----------------
;--- 4SAIL parameters
;-----------------

;--- atmospheric transmittance and percent of diffuse light (20% to 50%)
rg_rgextatm = 0.251 + 0.509
skyl_tab = RANDOM(SEED, n_sim, 20d0, 50d0, normal_dist=normal_dist)
IF param_flag[WHERE(param_list EQ 'skyl')] EQ 0 AND $
   altogether_flag EQ 1 THEN $
      skyl_tab = REPLICATE(40d0, n_sim)

;--- lai (0 to 6 m2/m2) (NEVER CONSTANT)
lai_tab = RANDOM(SEED, n_sim, 0d0, 6d0, normal_dist=normal_dist) > 0.

;--- leaf angle (0 to 90 deg)
leaf_angle_tab = RANDOM(SEED, n_sim, 0d0, 90d0, normal_dist=normal_dist)
IF param_flag[WHERE(param_list EQ 'leaf_angle')] EQ 0 AND $
   altogether_flag EQ 1 THEN $
      leaf_angle_tab = REPLICATE(20d0, n_sim)

;--- hot spot (>0, i.e. leaf dim/canopy height) (0.01 to 0.3)
hspot_tab = RANDOM(SEED, n_sim, 0.01d0, 0.3d0, normal_dist=normal_dist)
IF param_flag[WHERE(param_list EQ 'hspot')] EQ 0 AND $
   altogether_flag EQ 1 THEN $
      hspot_tab = REPLICATE(0.05d0, n_sim)

;--- soil coefficient (i.e. fraction of dry soil) (0.1 to 0.4)
psoil_tab = RANDOM(SEED, n_sim, 0d0, 0.4d0, normal_dist=normal_dist)
IF param_flag[WHERE(param_list EQ 'psoil')] EQ 0 AND $
   altogether_flag EQ 1 THEN $
      psoil_tab = REPLICATE(0.2d0, n_sim)

;-----------------
;--- PROSPECT parameters
;-----------------

;--- Cab ( 0 to 80 ug/cm2)  (NEVER CONSTANT but possibly correlated to LAI)
Cab_tab = RANDOM(SEED, n_sim, 0d0, 80d0, normal_dist=normal_dist)
IF correlation_flag THEN $
   Cab_tab = $
   RANDOM(SEED, n_sim, 0.0d0, 80d0, normal_dist=normal_dist) * lai_tab / max(lai_tab)

;--- Car (0.1-0.5Cab) ( 0 to 40 ug/cm2)  (NEVER CONSTANT but possibly correlated to Cab)
Car_tab = RANDOM(SEED, n_sim, 0d0, 40d0, normal_dist=normal_dist)
IF correlation_flag THEN $
   Car_tab = $
   (0.05d0 + RANDOM(SEED, n_sim, 0.0d0, 0.35d0, normal_dist=normal_dist)) * Cab_tab

;--- Cm (0.002 to 0.016, i.e. 20g/cm2 to 160g/cm2)
Cm_tab = RANDOM(SEED, n_sim, 0.002d0, 0.016d0, normal_dist=normal_dist)
IF param_flag[WHERE(param_list EQ 'Cm')] EQ 0 AND $
   altogether_flag EQ 1 THEN $
      Cm_tab = REPLICATE(0.008d0, n_sim)

;--- Cw (0.01-0.1 cm of water)
Cw_tab = RANDOM(SEED, n_sim, 0.01d0, 0.10d0, normal_dist=normal_dist)
IF param_flag[WHERE(param_list EQ 'Cw')] EQ 0 AND $
   altogether_flag EQ 1 THEN $
      Cw_tab = REPLICATE(0.04d0, n_sim)

;--- structural parameter (1 to 3.5)
Ns_tab = RANDOM(SEED, n_sim, 1d0, 3.5d0, normal_dist=normal_dist)
;IF correlation_flag THEN $
;   Ns_tab = 1.0d0 + 2.5d0 * ((5d0/Cab_tab)<1d0) * RANDOMU(SEED, n_sim)
IF param_flag[WHERE(param_list EQ 'Ns')] EQ 0 AND $
   altogether_flag EQ 1 THEN $
      Ns_tab = REPLICATE(1.8d0, n_sim)

;--- Cbrown (0 to 6)
Cbrown_tab = RANDOM(SEED, n_sim, 0.0d0, 6d0, normal_dist=normal_dist)
;IF correlation_flag THEN $
;   Cbrown_tab = ((Ns_tab-1d0)>0d0) * (0.3 + 1.8d0 * RANDOMU(SEED, n_sim))
IF param_flag[WHERE(param_list EQ 'Cbrown')] EQ 0 AND $
   altogether_flag EQ 1 THEN $
      Cbrown_tab = REPLICATE(0.02d0, n_sim)

;############################
; MAIN LOOP ON PARAMETERS
;############################
n_param2 = 1
IF altogether_flag EQ 0 THEN n_param2 = n_param
FOR ip2=0,n_param2-1 DO BEGIN

;----------------
;--- eventually set some parameters constant
;----------------

IF altogether_flag EQ 0 THEN BEGIN
   param_flag = REPLICATE(0, n_param)
   param_flag[ip2] = 1
   print,'Parameter '+param_list[ip2]
ENDIF
FOR ip=0, n_param-1 DO BEGIN
   cmd = param_list[ip] + '_tab2 = ' + param_list[ip] + '_tab'
   IF param_flag[ip] EQ 0 THEN $
      cmd = param_list[ip] + '_tab2 = REPLICATE(MEAN(' + param_list[ip] + '_tab), n_sim)'
   res = EXECUTE(cmd)
ENDFOR

;############################
; MAIN LOOP ON SIMULATIONS
;############################

IF restore_flag EQ 0 THEN BEGIN
   RGB_signal = FLTARR(3, n_sim)
   RGB_signal300 = FLTARR(3, n_sim)
   NDVI_signal = FLTARR(n_sim)
   FOR i_sim=0L,n_sim-1 DO BEGIN

      IF altogether_flag THEN print,'Simulation # '+AUTOSTRING(i_sim)

      ;--- input for SAIL
      angl  = leaf_angle_tab2[i_sim]          ;-- average leaf angle (°)
      lai   = lai_tab2[i_sim]                 ;-- leaf area index
      tts   = 90d - sun_height_tab2[i_sim]    ;-- solar zenith angle (°)
      tto   = webcam_zenith_angle_tab2[i_sim] ;-- observer zenith angle (°)
      psi   = webcam_azim_angle $
              - sun_azim_tab[i_sim]           ;-- azimuth (°) (sign not important?)
      Rg = rg_extatm_tab[i_sim] * rg_rgextatm ;-- global radiation
      skyl = skyl_tab2[i_sim]                 ;-- percentage of diffuse light
      psoil = psoil_tab2[i_sim]               ;-- fraction of dry soil 
      hspot = hspot_tab2[i_sim]               ;-- hot spot parameter

      ;--- input for PROSPECT5
      Cab = Cab_tab2[i_sim]       ;-- chlorophyll content (0-100 µg.cm-2)
      Car = Car_tab2[i_sim]       ;-- carotenoid content (0-50 µg.cm-2)
      Cbrown = Cbrown_tab2[i_sim] ;-- brown pigment content (arbitrary units) - wood/shade?
      Cw = Cw_tab2[i_sim]         ;-- equivalent water thickness (0.004-0.04cm)
      Cm = Cm_tab2[i_sim]         ;-- LMA (g.cm-2) (0.0019-0.0165 possible for Ohia)
      Ns = Ns_tab2[i_sim]         ;-- structure coefficient (1-3)

      ;--- output from PRO4SAIL
      ;    resh : hemispherical reflectance
      ;    resv : directional reflectance
      PRO4SAIL_5b,$
         Cab,Car,Cbrown,Cw,Cm,Ns,$
         angl,lai,hspot,tts,tto,psi,psoil,skyl,$
         resh,resv, $
         ;--- input/output ADDED BY JO
         Rg=Rg,$
         l=l, Idir=Idir, Idif=Idif, $
         Qabs_VIS=Qabs_VIS
  
      ;--- compute input signal to the camera
      I_tot = Idir + Idif       ;--- W/m2/m
      ii = WHERE(l LE l_IR_filter AND l GE l_UV_filter, complement=jj)
      analog2digit = 1d0 ;1d-9 / (n_avogadro*1e-6*h_planck*c_light/(l   *1e-9))
  
      ;--- interpolate down to 300nm
      l300 = [300+findgen(MIN(l)-300), l]
      resv300 = INTERPOL(resv[0,*], l, l300)
      I_tot300 = INTERPOL(I_tot, l, l300)
      ii300 = WHERE(l300 LE l_IR_filter  AND l300 GE l_UV_filter,complement=jj300)
      analog2digit300 = 1d0 ;1d-9 / (n_avogadro*1e-6*h_planck*c_light/(l300*1e-9))

      FOR ic=0,n_RGB-1-add_NDVI_flag DO BEGIN
         qy = INTERPOL(REFORM(quantum_yield[*,ic]), $
                       l_quantum_yield, l, /SPLINE) > 0. ;--- mol(e)/mol(photons)
         qy300 = INTERPOL(REFORM(quantum_yield[*,ic]), $
                          l_quantum_yield, l300, /SPLINE) > 0. ;--- mol(e)/mol(photons)
         qy[jj] = 0.
         qy300[jj300] = 0.
         IF ic EQ 0 THEN qy300_tab = qy300 ELSE qy300_tab = [[qy300_tab],[qy300]]
         ;---
         RGB_signal[ic,i_sim]    = $
            TOTAL(qy[ii]       * resv[0,ii]     * I_tot[ii]       * analog2digit[ii]      ) * $
            RGB_white_balance[ic]
         RGB_signal300[ic,i_sim] = $
            TOTAL(qy300[ii300] * resv300[ii300] * I_tot300[ii300] * analog2digit300[ii300]) * $
            RGB_white_balance[ic]
      ENDFOR

      ;--- compute NDVI
      ;ii_VIS = WHERE(l GE 580 AND l LE  680) ;--- AVHRR RED
      ;ii_NIR = WHERE(l GE 725 AND l LE 1100) ;--- AVHRR NIR
      ii_VIS = WHERE(l GE 545 AND l LE  565) ;--- MODIS RED
      ii_NIR = WHERE(l GE 841 AND l LE  871) ;--- MODIS NIR
      ;---
      VIS = TOTAL(resh[0,ii_VIS] * I_tot[ii_VIS]) 
      NIR = TOTAL(resh[0,ii_NIR] * I_tot[ii_NIR])
      NDVI_signal[i_sim] = (NIR - VIS) / (VIS + NIR)
      
   ENDFOR  ;--- i_sim loop

   ;--- normalise
   fRGB = RGB_signal
   FOR ic=0,n_RGB-1-add_NDVI_flag DO BEGIN
      fRGB[ic,*] = RGB_signal[ic,*] / TOTAL(RGB_signal[*,*],1)
   ENDFOR
   RGB_signal = fRGB
   IF add_NDVI_flag THEN RGB_signal = [RGB_signal, TRANSPOSE(NDVI_signal)]
   ;---
   fRGB = RGB_signal300
   FOR ic=0,n_RGB-1-add_NDVI_flag DO BEGIN
      fRGB[ic,*] = RGB_signal300[ic,*] / TOTAL(RGB_signal300[*,*],1)
   ENDFOR
   RGB_signal300 = fRGB
   IF add_NDVI_flag THEN RGB_signal300 = [RGB_signal300, TRANSPOSE(NDVI_signal)]
ENDIF ELSE BEGIN
   ip2_backup = ip2
   RESTORE, restore_file
   restore_flag = 1
   ip2 = ip2_backup
ENDELSE

;########################
;   PLOT CORRELATIONS
;########################

;--- define eps size
nxplot = n_param
IF altogether_flag EQ 1 THEN ii = WHERE(param_flag EQ 1,nxplot)
nyplot = n_RGB+1
;---
xboxspace = 0.0
xboxsize = 1.8
xboxleft = 1.0
xboxright = 0.3
;---
yboxspace = 0.0
yboxsize = 2.6
yboxbot = 0.65
yboxtop = 0.65
xsize = xboxright + xboxleft + xboxsize * nxplot + xboxspace*((nxplot-1)>0)
ysize = yboxbot   + yboxtop  + yboxsize * nyplot + yboxspace*((nyplot-1)>0)

;--- define plot coordinates
nplot = nxplot*nyplot
xbox_size = replicate(xboxsize/xsize,nxplot)
ybox_size = replicate(yboxsize/ysize,nyplot)
xbox_min = fltarr(nxplot)
ybox_max = fltarr(nyplot)
xbox_min[0] = xboxleft/xsize
ybox_max[0] = 1.-yboxtop/ysize
for iplot=1,nxplot-1 do xbox_min[iplot] = $
   xbox_min[iplot-1] + xbox_size[iplot-1] + xboxspace/xsize
for iplot=1,nyplot-1 do ybox_max[iplot] = $
   ybox_max[iplot-1] - ybox_size[iplot-1] - yboxspace/ysize
IF ip2 EQ 0 THEN !p.multi = [0,nxplot,nyplot]

;--- define plot parameters
charsize = 1.
symsize = 0.1
charsize_leg = 0.7
line_thickness  = 2
ccg_symbol,sym=1,fill=0

;--- define title
title0 = stdfont + '!D'+$
         autostring(FIX(longitude180))+s_degree+stdfont+'W - ' +$
         autostring(FIX(latitude))+s_degree+stdfont+'N'
IF param_flag[0] EQ 0 AND altogether_flag EQ 1 THEN $
   title0 = title0+' - ' + $
            s_theta+stdfont+'!isun!d='+autostring(FIX(MEAN(sun_height_tab2)))+s_degree+stdfont
            ;;;autostring(FIX(time))+'A.M.'
title0 = title0+'!c!d' +$
         camera_type+' camera'

;--- loop on parameters
ipminus=0
iip = WHERE(param_flag EQ 1,nii)
FOR i = 0, nii-1 DO BEGIN
   ip = iip[i]

   ;--- open (temporary) ps file
   IF ip2 EQ 0 AND i EQ 0 THEN BEGIN
      set_plot, 'ps'
      !p.font=0
      device, filename=file, encapsulated=1, /helvetica, /color
      device, xsize=xsize, ysize=ysize
   ENDIF

   ;--- define xarr, xtitle, xrange
   xtitle0 = stdfont+param_title[ip]
   cmd = 'xarr = ' + param_list[ip] + '_tab2'
   res = EXECUTE(cmd)
   IF STRMID(param_title[ip],0,3) EQ 'EWT' THEN BEGIN
      xarr = xarr*10
      xtitle0 = xtitle0 ;+ ' ('+s_multiplication+stdfont+'100)'
   ENDIF
   IF STRMID(param_title[ip],0,3) EQ 'LMA' THEN BEGIN
      xarr = xarr*1e4
      xtitle0 = xtitle0 ;+ ' ('+s_multiplication+stdfont+'1000)'
   ENDIF
   xrange = [MIN(xarr),max(xarr)]
   yrange_tab = [[0.27, 0.57],$
                 [0.30, 0.48],$ ;[0.22, 0.52],$
                 [0.13, 0.43],$
                 [0.35, 0.95]]
   IF phenology_period NE 'allseason' THEN BEGIN
      yrange_tab[1,0] = 0.48
      yrange_tab[1,2] = 0.40
   ENDIF

   ;--- do the plots
   FOR iyplot = 0,nyplot-1 DO BEGIN
      ixplot = ip2 + i - ipminus
      ic = nyplot-2-iyplot

      ;--- define yarr, ytitle, yrange
      IF iyplot NE nyplot-1 THEN BEGIN
         ytitle = RGB_title_TAB[ic]+' fraction!n'
         yarr = RGB_signal[ic,*]
         yrange = yrange_tab[*,ic]
      ENDIF ELSE BEGIN
         ytitle = 'Frequency'
         n_bin = 60 ;--- max 60 because of bar_plot routine...
         yarr = HISTOGRAM(xarr, $
                          BINSIZE=((max(xarr)-min(xarr))/(n_bin-1))>0.001d, $
                          LOCATIONS=hlocs)
         yrange = [0,ROUND(6*n_sim/n_bin)]
      ENDELSE
      ytickname=''
      ytickformat='(F5.2)'
      IF iyplot EQ nyplot-1 THEN ytickformat = '(I3)'
      IF ixplot GT 0 THEN BEGIN
         ytitle = ''
         ytickname = REPLICATE(' ',10)
         ytickformat=''
      ENDIF

      ;--- define position
      position = [xbox_min[ixplot]                  , $
                  ybox_max[iyplot]-ybox_size[iyplot], $
                  xbox_min[ixplot]+xbox_size[ixplot], $
                  ybox_max[iyplot]]

      ;--- draw a frame
      IF iyplot NE nyplot-1 THEN BEGIN
         xtitle = ''
         xtickname = REPLICATE(' ',10)
      ENDIF ELSE BEGIN
         xtitle = xtitle0
         xtickname = ''
      ENDELSE
      IF iyplot EQ 0 THEN title = title0 ELSE title=''
      CASE param_list[ip] OF
         'Cm': xtickinterval=50
         'hspot': xtickinterval=0.10
         ELSE: xtickinterval=0
      ENDCASE
      plot,[0],[0],$
           title=title,$
           charsize=charsize,charthick=charthick,position=position,$
           xthick=axis_thickness,ythick=axis_thickness,xminor=1,yminor=1,$
           xtickinterval=xtickinterval,$
           xrange=xrange, xtitle=xtitle, xstyle=1, xtickname=xtickname,$
           yrange=yrange, ytitle=ytitle, ystyle=1, ytickname=ytickname,$
           ytickformat=ytickformat,$
           ;xticks=5,yticks=5,$
           font=!p.font

      ;--- plot data
      IF iyplot NE nyplot-1 THEN BEGIN
         oplot,xarr,yarr, psym=8,symsize=symsize, color=150 ;220 ;;;RGB_color_TAB[ic]
         IF altogether_flag EQ 0 THEN $
            oplot,[MEAN(xarr),MEAN(xarr)], $
                  !y.crange, linestyle=1,thick=2,color=8

         ;--- plot fit
         n_fit = 12
         xfit =  FLTARR(n_fit)
         yfit =  REPLICATE(missval,n_fit)
         yplus = FLTARR(n_fit)
         yminu = FLTARR(n_fit)
         FOR j=0,n_fit-1 DO BEGIN
            jj = WHERE(xarr GE !x.crange[0]+FLOAT(j  )/n_fit*(!x.crange[1]-!x.crange[0]) AND $
                       xarr LE !x.crange[0]+FLOAT(j+1)/n_fit*(!x.crange[1]-!x.crange[0]),njj)
            IF njj GT 1 THEN BEGIN
               xfit[j]  = !x.crange[0]+FLOAT(j+0.5)/n_fit*(!x.crange[1]-!x.crange[0])
               ;yfit[j]  =   MEAN(yarr[jj])
               ;yplus[j] = STDDEV(yarr[jj]) + yfit[j]
               ;yminu[j] = STDDEV(yarr[jj]) - yfit[j]
               yfit[j]  =   MEDIAN(yarr[jj])
               yband = ogee_extrema(REFORM(yarr[jj]), $
                                    min_percent =  5., $
                                    max_percent = 95., $
                                    missval = missval)
               yplus[j] = yband[1]
               yminu[j] = yband[0]
            ENDIF
         ENDFOR
         ii = WHERE(yfit NE missval)
         oplot, xfit[ii], yfit[ii], thick=4, linestyle=0, color=RGB_color_TAB[ic]
         oplot, xfit[ii], yplus[ii], thick=2, linestyle=2, color=RGB_color_TAB[ic]
         oplot, xfit[ii], yminu[ii], thick=2, linestyle=2, color=RGB_color_TAB[ic]
      ENDIF ELSE BEGIN
         ;oplot,hlocs, yarr, psym=10,symsize=symsize, color=0
         bar_plot,yarr,$
                 baselines=replicate(0,n_elements(hlocs)),$
                 colors=replicate(150,n_elements(hlocs)), $
                 /overplot;,/outline
      ENDELSE

      skip_plot:

   ENDFOR ;--- iyplot

   ;--- close (temporary) PS file
   IF i EQ nii-1 AND ip2 EQ n_param2-1 THEN BEGIN
      device, /close
      set_plot, 'x'
      !p.font=-1
   ENDIF

   ;--- converts to PDF and deletes PS file (for Mac only)
   IF i EQ nii-1 AND ip2 EQ n_param2-1 THEN BEGIN
      cmd = 'pstopdf '+file+' -o '+(STR_SEP(file,'.ps'))[0]+'.pdf'
      ;cmd = 'ps2pdf '+file+' '+(STR_SEP(file,'.ps'))[0]+'.pdf'
      spawn,cmd
      FILE_DELETE, file
   ENDIF

ENDFOR ;--- i loop

ENDFOR ;--- ip2 loop

;--- save session
IF restore_flag EQ 0 AND altogether_flag EQ 1 THEN SAVE, /ALL, FILENAME=restore_file
 
!p.font=0
!p.multi=0
stop
stop
END

