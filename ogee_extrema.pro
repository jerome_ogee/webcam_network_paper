;++
;===================================================================================
;///////////////////////////////////////////////////////////////////////////////////
;
; GENERAL DESCRIPTION
;
; NAME:
;   ogee_extrema
;
; PURPOSE:
;   Function to compute extrema (min and max) of an array, once
;   removed missing values and values below or above a certain
;   percentage of extremes in the histogram of all values
;
; CALLING SEQUENCE:
;   res = ogee_extrema(var, $
;                      min_percent = min_percent, $
;                      max_percent = max_percent, $
;                       missval = missval)
;
; INPUTS:
;    var                  = array to be analysed
;
; OPTIONAL INPUT PARAMETERS:
;    min_percent          = percentage of smallest values to be thrown away in
;                           the analysis
;    max_percent          = percentage of largest values to be thrown away in
;                           the analysis
;    missval              = missing values to be removed from the analysis
;
; OUTPUTS:
;    None.
;
; RESTRICTIONS:
;    None.
;
;///////////////////////////////////////////////////////////////////////////////////
;===================================================================================

FUNCTION ogee_extrema,var, $
                      min_percent = min_percent, $
                      max_percent = max_percent, $
                      missval = missval

;--- define optional flags if not specified
IF (SIZE(min_percent,/TYPE) EQ 0) THEN min_percent = 0.
IF (SIZE(max_percent,/TYPE) EQ 0) THEN max_percent = 100.
IF (SIZE(missval,/TYPE) EQ 0) THEN missval = -9999.

;--- calculate dimensions
temp = var
ii = WHERE(var NE missval AND FINITE(var), nii)
n_bin = 10000
IF nii GT 0 THEN BEGIN
   var = temp[ii]
   omin  = MIN(var,MAX=omax) & omax = omax>omin+1e-4
   bin   = (omax-omin)/FLOAT(n_bin)
   histo = HISTOGRAM(var,BINSIZE=bin)
   N     = N_ELEMENTS(var)
   chisto = histo
   FOR i=1L, N_ELEMENTS(histo)-1 DO chisto[i] = chisto[i-1] + histo[i]
   im = (min_percent/100.)*N & i_min = 0L  &  WHILE (chisto[i_min] LE im AND i_min LT n_bin-1) DO i_min = i_min+1
   im = (max_percent/100.)*N & i_max = 0L  &  WHILE (chisto[i_max] LT im AND i_max LT n_bin-1) DO i_max = i_max+1
   min = omin + bin*i_min
   max = omin + bin*i_max
ENDIF ELSE BEGIN
   min = missval
   max = missval
ENDELSE
var = temp

RETURN,[min, max]

END
;+-

