# README #

### What is this repository for? ###

This repository archives the IDL code and data required to generate Figs. 11-14 in Wingate et al. (Biogeosciences, 2015).

### How do I get set up? ###

To run this code, you also need:

- PROSAIL IDL routines (https://bitbucket.org/jerome_ogee/prosail_lib/overview)

- an IDL license (although the code can run in demo mode)


The command line is then:

- "IDL < batch_idl_sensitivity" to run the sensitivity tests

- "IDL < batch_idl_timeseries" to run the timeseries

### Contribution guidelines ###

The code must remain open-source. If part of this code is used in a publication, please cite Wingate et al. (Biogeosciences, 2015) for a reference. Data to test the code on Alice Holt forest remain the property of Forest Research, UK.

### Who do I talk to? ###

If you need help with this repository, please contact: jerome.ogee@inra.fr