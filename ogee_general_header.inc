;--- system variables
idx = (WHERE([ 'MacOS', 'Windows', 'unix' ] eq !version.os_family))[0]
os = ([ 'MacOS', 'Windows', 'unix' ])[idx]   ;- operating system's name
dl = ([ ':', '\', '/' ])[idx]                ;- path/files delimiters
wdir = ([ 'PWD', 'CD', 'PWD' ])[idx]         ;- environment variable for current working directory
home = ([ 'HOME', 'HOMEPATH', 'HOME' ])[idx] ;- environment variable for home directory
ls_cmd = ([ 'LS ', 'DIR /B ', 'LS ' ])[idx]  ;- shell command for listing current directory

;--- set default fonts
!P.FONT=0 ;--- to select DEVICE/HARDWARE fonts
stdfont = '!3'
stdfont_bold = '!6'
symfont = '!9'
stdfont_italic = '!14'

;--- graphic symbols
s_degree     = symfont+string(byte(176))+stdfont_bold
s_peclet     = symfont+string(byte(195))+stdfont_bold
s_plusminus  = symfont+string(byte(177))+stdfont_bold
s_abs        = symfont+string(byte(124))+stdfont_bold
s_permil     =         string(byte(189))+stdfont_bold
s_different  = symfont+string(byte(185))+stdfont_bold
s_derivative = symfont+string(byte(182))+stdfont_bold
s_multiplication = symfont+string(byte(180))+stdfont_bold
;---
s_leftarrow  = symfont+string(byte(172))+stdfont_bold
s_downarrow  = symfont+string(byte(175))+stdfont_bold
s_uparrow    = symfont+string(byte(173))+stdfont_bold
s_rightarrow = symfont+string(byte(174))+stdfont_bold
;---
s_alfa       = symfont+string(byte(97))+stdfont_bold
s_beta       = symfont+string(byte(98))+stdfont_bold
s_phi        = symfont+string(byte(102))+stdfont_bold
s_gama       = symfont+string(byte(103))+stdfont_bold
s_delta      = symfont+string(byte(100))+stdfont_bold
s_epsilon    = symfont+string(byte(101))+stdfont_bold
s_kappa      = symfont+string(byte(107))+stdfont_bold
s_lambda     = symfont+string(byte(108))+stdfont_bold
s_micro      = symfont+string(byte(109))+stdfont_bold
s_rho        = symfont+string(byte(114))+stdfont_bold
s_theta      = symfont+string(byte(113))+stdfont_bold
s_sigma      = symfont+string(byte(115))+stdfont_bold
s_tau        = symfont+string(byte(116))+stdfont_bold
s_psi        = symfont+string(byte(121))+stdfont_bold
;---
s_bigdelta   = symfont+string(byte(068))+stdfont_bold
s_biggama    = symfont+string(byte(071))+stdfont_bold
;---
axis_thickness = 1

;--- store default color tables to reset whenever we want to
TVLCT,red,green,blue,/GET

;--- constants
one_day = 86400.
ndays_per_week = 7.
tk0 = 273.15
r_gas = 8.31441
tk25 = 298.15
l_vap = 2.45e6
cp_air = 1010.
rho_air = 1.2
molar_weight_air = 29e-3
molar_weight_h2o = 18e-3
molar_weight_carbon = 12e-3
mol2joule = molar_weight_h2o*l_vap
density_w = 1000.
stefan = 5.67e-8
cte_grav = 9.81

;--- absolute isotope ratios of international standards
R18O_VPDB_CO2 = 0.00208835d0        ;--- after Allison et al. 1995
R18O_VSMOW    = 0.002005d0          ;--- after Gonfiantini 1978
;R18O_VPDB     = 0.002079d0         ;--- after Craig 1957 cited by Allison et al. 1995
R18O_VPDB     = R18O_VSMOW*(1.0309) ;--- after Allison et al. 1995
R13C_VPDB     = 0.0112372           ;--- after Craig 1957 cited by Allison et al. 1995
VPDBCO2_to_VSMOW = R18O_VPDB_CO2 / R18O_VSMOW

;--- general data file parameters
missval = -9999.
missrec = !VALUES.F_NAN
separator = ';'
separator2 = ','
CSV_separator_FR = ';'
CSV_separator_EN = ','

;--- main paths and file variables
path_data = GETENV(home)+dl+'WORK'+dl+'DATA'+dl
path_data_local = GETENV(wdir)+dl+'data_files'+dl
path_out  = GETENV(wdir)+dl+'ps_files'+dl
path_out_ascii      = GETENV(wdir)+dl+'ascii_files'+dl
path_out_postscript = GETENV(wdir)+dl+'ps_files'+dl
